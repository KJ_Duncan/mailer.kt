package view

import controller.IMAPController
import events.SentViewEvent
import javafx.scene.control.TableView
import model.Sentbox
import tornadofx.*

class SentTableView : View() {
  private val imapController: IMAPController by inject()

  override val root: TableView<Sentbox> = tableview(imapController.imapSentBoxObservable()) {
    readonlyColumn("Date", Sentbox::date)
    readonlyColumn("To", Sentbox::receiver)
    readonlyColumn("Subject", Sentbox::subject).remainingWidth()
    smartResize()
    onUserSelect { fire(SentViewEvent(it)) }
  }
}
