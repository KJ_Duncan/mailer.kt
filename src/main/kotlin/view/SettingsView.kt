package view

import io.Console
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleStringProperty
import javafx.geometry.Pos
import javafx.scene.input.KeyCode
import javafx.scene.paint.Color
import javafx.util.Duration
import prop.PropertiesWrapper
import prop.RenderHTML.*
import prop.UserSettingProperty.*
import tornadofx.*
import tornadofx.controlsfx.infoNotification
import javax.mail.internet.InternetAddress

class SettingsView : View("Mailer settings") {
  private val alias = SimpleStringProperty()
  private val bool = SimpleBooleanProperty(false)

  /* NOTE:[SettingsView::View] controlsfx PropertySheet.java
      <https://github.com/controlsfx/controlsfx/blob/master/controlsfx/src/main/java/org/controlsfx/control/PropertySheet.java> */
  override val root = form {
    fieldset {
      spacing = 15.0
      alignment = Pos.CENTER

      /* TEST:[SettingsView] host service connected text colour */
      field("Host service") {
        // PropertiesWrapper.props.getProperty("mail.imaps.host").toUpperCase()
        text("Outlook".toUpperCase()) {
          // if (imapController.isConnected()) {
          //   fill = Color.GREEN
          // } else {
          //   fill = Color.RED
          // }
          fill = Color.GREEN
        }
      }

      field("Email address") {
        togglegroup {
          radiobutton("Default") {
            action {
              bool.value = false
              alias.value = ""
            }
          }.fire()

          radiobutton("Alias") {
            action { bool.value = true }
          }
        }
      }

      field("Alias email address") {
        enableWhen(bool)
        textfield(alias) {
          /* TEST:[alias email] check if throws before or during user input,
               commit the text field on key press enter */
          setOnKeyReleased { keyEvent ->
            if (keyEvent.code.name == KeyCode.ENTER.name) {
              if (checkEmailAddress(alias.valueSafe)) {
                PropertiesWrapper.props[ALIAS_EMAIL_string.key()] = alias.valueSafe
              } else {
                alias.value = "Non valid address."
                this.selectAll()
              }
            }
            else return@setOnKeyReleased
          }
            // check(EmailAddressValidator.isValid(alias.valueSafe)
        }
      }

      field("Open attachments") {
        togglegroup {
          radiobutton("Yes") {
            action { PropertiesWrapper.props[OPEN_ATTACHMENT_boolean.key()] = true }
          }
          radiobutton("No") {
            action { PropertiesWrapper.props[OPEN_ATTACHMENT_boolean.key()] = false }
          }.fire()
        }
      }

      field("Message html") {
        togglegroup {
          radiobutton("Plain") {
            action { PropertiesWrapper.props[RENDER_HTML_api.key()] = Plain }
          }.fire()
          radiobutton("Format") {
            action { PropertiesWrapper.props[RENDER_HTML_api.key()] = Format }
          }
          radiobutton("Format with Images") {
            action { PropertiesWrapper.props[RENDER_HTML_api.key()] = Images }
          }
        }
      }

      hyperlink("Read rendering a messages HTML requires care") {
        action {
          infoNotification(
            "\t    Guideline 3-3 / INJECT-3: XML and HTML generation requires care",
            "\nUntrusted data should be properly sanitized before being included in HTML or XML output.\n" +
                "Failure to properly sanitize the data can lead to many different security problems,\n" +
                "such as Cross-Site Scripting (XSS) and XML Injection vulnerabilities...\n\n" +
                "Oracle 2019, Secure Coding Guidelines for Java SE, Updated for Java SE 13, Document version: 7.3,\n" +
                "viewed on 16 Dec 2019, https://www.oracle.com/technetwork/java/seccodeguide-139067.html#3-3\n",
            Pos.TOP_RIGHT,
            Duration.seconds(15.0)
          )
        }
      }
    }
  }

  private fun checkEmailAddress(address: String): Boolean =
    Console.isSuccess("SettingsView checking users alias address") {
      InternetAddress(address).validate()
    }

  override fun onUndock() {
    /* REMOVE:[SettingsView] Console.showInfo and uncomment PropertiesWrapper.props */
    Console.showInfo("Attachment: ${PropertiesWrapper.props[OPEN_ATTACHMENT_boolean.key()] as Boolean}")
    Console.showInfo("Render Html: ${PropertiesWrapper.props[RENDER_HTML_api.key()].toString()}")
    Console.showInfo("Email: ${alias.value}")
  }
}
