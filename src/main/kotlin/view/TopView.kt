package view

import controller.MasterViewController
import events.Logout
import events.SendEvent
import javafx.beans.property.SimpleObjectProperty
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.geometry.Pos
import javafx.scene.layout.HBox
import javafx.stage.StageStyle.UTILITY
import javafx.util.Duration
import tornadofx.*

class TopView : View() {
  private val masterViewController: MasterViewController by inject(FX.defaultScope)
  private val settingsView: SettingsView by inject(FX.defaultScope)

  /* REMOVE:[connectionFrequency] remove value '1 min' */
  private val connectionFrequency: ObservableList<String> =
    FXCollections.observableArrayList("20 min", "1 hour", "2 hour", "3 hour", "1 min")
  private val selectedFrequency: SimpleObjectProperty<String> = SimpleObjectProperty(connectionFrequency[0])
  /* FIXME:[connectionMap] can we use ObservableMap instead? enum values for type safety */
  private val connectionMap: Map<String, Duration> = mapOf(
    connectionFrequency[0] to Duration.minutes(20.0),
    connectionFrequency[1] to Duration.hours(1.0),
    connectionFrequency[2] to Duration.hours(2.0),
    connectionFrequency[3] to Duration.hours(3.0),
    connectionFrequency[4] to Duration.minutes(1.0)
  )

  override val root: HBox = hbox {
    hboxConstraints {
      alignment = Pos.TOP_CENTER
      paddingAll = 5.0
      spacing = 15.0
    }

    label("Check every") {
      paddingTop = 5.0
      paddingLeft = 5.0
    }
    combobox(selectedFrequency, connectionFrequency) {
      setOnAction { masterViewController.scheduleInbox(connectionMap.getValue(selectedFrequency.value)) }
    }

    hbox {
      alignment = Pos.TOP_CENTER
      spacing = 15.0
      paddingRight = 10.0

      button("Inbox F7") {
        action { masterViewController.checkInboxView() }
        shortcut("F7")
      }

      button("Outbox F8") {
        action { masterViewController.checkSentView() }
        shortcut("F8")
      }

      button("New F4") {
        action { fire(SendEvent) }
        shortcut("F4")
      }

      button("Settings F1") {
        action { settingsView.openModal(stageStyle = UTILITY) }
        shortcut("F1")
      }

      button("Disconnect F12") {
        action { fire(Logout(quit = false)) }
        shortcut("F12")
      }

      button("Quit F6") {
        action { fire(Logout(quit = true)) }
        shortcut("F6")
      }
    }
  }
}
