package view

import javafx.scene.layout.BorderPane
import tornadofx.*

/**
 * Application remains open and ItemViewModel holds the viewable list of emails
 * sent during runtime.
 */
class MasterView : View("Mailer: a light weight email application") {
  private val topView: TopView by inject(FX.defaultScope)
  private val centerView: CenterView by inject(FX.defaultScope)

  override val root: BorderPane = borderpane {
    top {
      // addClass(Styles.masterViewTop)
      add(topView)
    }

    /* TODO:[MainView] style.css max and min size */
    // https://docs.oracle.com/javase/8/javafx/layout-tutorial/size_align.htm#JFXLY133
    center {
      add(centerView)
    }
  }
}
