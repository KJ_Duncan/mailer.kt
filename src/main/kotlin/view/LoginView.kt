package view

import controller.LoginController
import events.Logout
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import javafx.geometry.Orientation
import javafx.geometry.Pos
import javafx.scene.control.Button
import prop.HostAccount.Gmail
import prop.HostAccount.Office365
import tornadofx.*
import java.util.Arrays

// https://github.com/edvin/github-browser/blob/master/src/main/kotlin/view/LoginScreen.kt
class LoginView : View("Connect to email account") {
  private val loginController: LoginController by inject(FX.defaultScope)
  private val host = SimpleObjectProperty<Any>()
  private val username = SimpleStringProperty()
  private val password = SimpleObjectProperty<CharArray>()


  override val root: Form = form {
    fieldset(labelPosition = Orientation.VERTICAL) {
      field("User name or email address") {
        textfield(username) {
          /* REMOVE:[LoginView] userModel.username = kjd016 */
          username.value = "kjd016@student.usc.edu.au"
          whenDocked { requestFocus() }
        }//.required()
      }
      // FIXME: java.lang.IllegalArgumentException: The addValidator extension can only be used on inputs that are already bound bidirectionally to a property in a Viewmodel. Use validator.addValidator() instead or make the property's bean field point to a ViewModel.
      field("Password") {
        /* NOTE: Strings are immutable and in java are placed permainatly in a String pool for copy on write action (good).
                 This is challenging for sensitive information such as passwords (bad). */

        /* TODO:[LoginView::Password] Research method override for garbage collection to clear String pool,
                use ide debugging to follow the heap/pool/stack. <-- thank you Jetbrains
                FX uses a single threaded environment which could be problematic for the above use-case. */

        /* TEST: Leave login alone cited as a final milestone  
                 Eventbus: what are the security implecations for events or scope messages for POKO's
                           auto PBE or MAC on whitelist emails, MessageDigest the rest??
 
                 https://www.oracle.com/technetwork/java/seccodeguide-139067.html
                 https://docs.oracle.com/javase/8/docs/technotes/guides/security/spec/security-specTOC.fm.html
                 https://docs.oracle.com/javase/tutorial/security/tour2/step1.html
                 https://docs.oracle.com/javase/tutorial/security/index.html */

        /* 
         * FIXME: Maybe more prudent to instantiate login via I/O from the command line. The problem is the `passwordfield` requires a String.
         * 
         *   val password: CharArray = System.console().readPassword("Password: ")
         * 
         *   `readPassword` returns a character array, not a String, 
         *   so the password can be overwritten, removing it from memory as 
         *   soon as it is no longer needed.
         *
         *   Check benefits include; a quick startup, smaller footprint, domain restricted
         *
         * @see https://docs.oracle.com/javase/tutorial/essential/io/cl.html
         *      https://docs.oracle.com/javase/tutorial/essential/io/examples/Password.java
         *
         *   object PasswordAuthenticator : Authenticator() {
         *     protected override fun getPasswordAuthentication() =
         *       PasswordAuthentication(username, password)
         *   }
         *
         * @see http://rosettacode.org/wiki/HTTPS/Authenticated#Kotlin
         *      http://rosettacode.org/wiki/Send_email#Kotlin
         *
         *   restrict CharArray min, max size and validate untrusted input data from the command line
         *
         *   remember the heap is FILO so be clever an use intellij on this one.
         */
        passwordfield(password.name)//.required()
      }
    }

    hbox {
      label("Host service")
      togglegroup(host) {
        radiobutton(Office365.OUTLOOK).fire()
        radiobutton(Gmail.GMAIL)
      }
    }

    hbox {
      alignment = Pos.CENTER
      paddingAll = 15.0
      spacing = 15.0

      button("Connect") {
        isDefaultButton = true
        action { login() }
      }
      button("Quit") {
        action { fire(Logout(quit = true)) }
      }
    }
  }

  /* NOTE: links
      review java secure coding guidlines <https://www.oracle.com/technetwork/java/seccodeguide-139067.html>
      section 9 Access Control https://www.oracle.com/technetwork/java/seccodeguide-139067.html#9
      Java 11 is now the minimum version for LTS (long term support)
      java 11 Access Control https://docs.oracle.com/en/java/javase/11/security/java-security-overview1.html#GUID-BBEC2DC8-BA00-42B1-B52A-A49488FCF8FE
      java 11 security overview https://docs.oracle.com/en/java/javase/11/security/java-security-overview1.html#GUID-2EF91196-D468-4D0F-8FDC-DA2BEA165D10 */
  private fun Button.login() {
    loginController.tryLogin(
      username = username.valueSafe,
      password = password.value,
      host = host.value.toString()
    )
  }

  override fun onDock() {
    nullOutCharArray()
  }

  override fun onUndock() {
    nullOutCharArray()
    host.value = ""
    username.value = ""
  }

  private fun nullOutCharArray(): Unit {
    Arrays.fill(password.value, ' ')
    password.value = null
  }
}
