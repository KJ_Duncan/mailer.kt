package view

import events.ReceivedEvent
import events.ReceivedFormEvent
import javafx.beans.property.SimpleObjectProperty
import javafx.geometry.Pos
import javafx.scene.layout.Priority
import javafx.scene.web.HTMLEditor
import model.ReceivedMessage
import tornadofx.*

class ReceivedFormFragment : Fragment("Received Email") {
  private val receivedProperty = SimpleObjectProperty<ReceivedMessage>()
  private var receivedMessage by receivedProperty

  private val htmlEditor: HTMLEditor =
    htmleditor {
      // prefHeight = 250.0
      vgrow = Priority.ALWAYS
      hgrow = Priority.ALWAYS
    }

  override val root: Form = form {
    // vboxConstraints { paddingAll = 15.0 }
    fieldset {
      field("Recieved:") {
        text(receivedMessage.date.toString())
      }

      field("Sender:") {
        text(receivedMessage.from.joinToString())
      }

      field("Subject:") {
        text(receivedMessage.subject)
      }

      field {
        htmlEditor.htmlText = receivedMessage.content
        // htmlEditor.isDisable = true
        this.add(htmlEditor)
      }

      hbox {
        alignment = Pos.CENTER_RIGHT
        spacing = 15.0
        paddingRight = 10.0

        button("Forward F8") {
          action {
            fire(ReceivedEvent(received = receivedMessage, forward = true))
          }
          shortcut("F8")
        }

        button("Reply F7") {
          action {
            fire(ReceivedEvent(received = receivedMessage, forward = false))
          }
          shortcut("F7")
        }
      }
    }
  }

  init {
    subscribe<ReceivedFormEvent> {
      receivedMessage = it.received
    }
  }
}
