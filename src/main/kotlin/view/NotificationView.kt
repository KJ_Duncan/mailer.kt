package view

import io.FileManager
import javafx.beans.property.SimpleStringProperty
import org.controlsfx.control.NotificationPane
import tornadofx.*
import tornadofx.controlsfx.notificationPane

/* REMOVE:[NotificationView] check if class not in use */
class NotificationView : View() {
  private val fileManager: FileManager by param()
  private val messageProperty: SimpleStringProperty = SimpleStringProperty()
  private var message: String by messageProperty

  override val root: NotificationPane = notificationPane {
    text = message
    // actions.add(Action())
  }

  fun setNotificationMessage(notification: String) {
    message = notification
  }

  override fun onUndock() {
    message = ""
  }
}
