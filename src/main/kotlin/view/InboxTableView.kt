package view

import controller.IMAPController
import events.ReceivedEvent
import events.ReceivedFormEvent
import javafx.scene.control.TableView
import model.Inbox
import model.ReceivedMessage
import tornadofx.*

class InboxTableView : View() {
  private val imapController: IMAPController by inject(FX.defaultScope)

  override val root: TableView<Inbox> = tableview(imapController.imapInboxObservable()) {
    readonlyColumn("Name", Inbox::folderName).contentWidth(padding = 200.0)
    readonlyColumn("Unread", Inbox::unread).contentWidth(padding = 50.0)
    smartResize()
    rowExpander {
      // tornadofx/ItemControls.kt:829
      // toggleExpanded()
      // shortcut(KeyCombination)
      // prefHeight = 120.0  // if smart resize does nothing use this
      tableview(it.messages.asObservable()) {
        readonlyColumn("Date", ReceivedMessage::date)
        readonlyColumn("From", ReceivedMessage::from)
        readonlyColumn("Subject", ReceivedMessage::subject).remainingWidth()
        smartResize()

        onUserSelect { received: ReceivedMessage -> fire(ReceivedFormEvent(received)) }

        contextmenu {
          item("Reply").action {
            selectedItem.apply {
              fire(ReceivedEvent(received = this ?: return@apply, forward = false))
            }
          }

          item("Forward").action {
            selectedItem.apply {
              fire(ReceivedEvent(received = this ?: return@apply, forward = true))
            }
          }

          item("Status").action {
            selectedItem.apply {
              // TODO:[TableView<Inbox>] a user option to set email flag to something seen/unseen/junk/...
            }
          }
        }
      }
    }
  }
}
