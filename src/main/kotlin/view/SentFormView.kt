package view

import events.SentEvent
import events.SentViewEvent
import javafx.beans.property.SimpleObjectProperty
import javafx.scene.layout.Priority
import model.Sentbox
import tornadofx.*

class SentFormView : View() {
  private val sentboxProperty = SimpleObjectProperty<Sentbox>()
  private var sentbox by sentboxProperty

  // https://youtu.be/BTZn9m_qMUI?t=143
  // override val scope: Scope = super.scope as EmailSenderScope

  override val root: Form = form {
    // vboxConstraints { paddingAll = 15.0 }
    fieldset {
      field("Date:") {
        text(sentbox.date.toString())
      }

      field("To:") {
        text(sentbox.receiver.joinToString())
      }

      field("Subject:") {
        text(sentbox.subject)
      }

      field {
        textarea(sentbox.content) {
          isEditable = false
          prefRowCount = 15
          vgrow = Priority.ALWAYS
        }
      }

      button("Forward F8") {
        action { fire(SentEvent(sentbox)) }
        shortcut("F8")
      }
    }
  }

  init {
    subscribe<SentViewEvent> {
      sentbox = it.sent
    }
  }
}
