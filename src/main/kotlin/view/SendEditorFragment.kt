package view

import controller.SendController
import io.Console
import javafx.beans.property.SimpleStringProperty
import javafx.scene.control.SelectionMode
import javafx.scene.layout.Priority
import javafx.scene.web.HTMLEditor
import javafx.stage.FileChooser
import model.SendMessageModel
import tornadofx.*
import util.whileEach
import java.io.File
import javax.mail.internet.InternetAddress

/**
 * Avoid referencing other UI elements directly. Refactor to `bind` your
 * UI elements to properties and manipulate the properties instead.
 *
 * The `ViewModel` provides easier ways to do this.
 *
 *
 * NOTE: docstring reference tornadofx/ViewModel.kt:74
 *
 * Wrap a JavaFX property and return the ViewModel facade for this property
 *
 * The value is returned in a lambda so that you can swap source objects
 * and call rebind to change the underlying source object in the mappings.
 *
 * You can bind a facade towards any kind of property as long as it can
 * be converted to a JavaFX property. TornadoFX provides a way to support
 * most property types via a concise syntax, see below for examples.
 * ```
 * class PersonViewModel(var person: Person) : ViewModel() {
 *     // Bind JavaFX property
 *     val name = bind { person.nameProperty() }
 *
 *     // Bind Kotlin var based property
 *     val name = bind { person.observable(Person::name) }
 *
 *     // Bind Java POJO getter/setter
 *     val name = bind { person.observable(Person::getName, Person::setName) }
 *
 *     // Bind Java POJO by property name (not type safe)
 *     val name = bind { person.observable("name") }
 * }
 * ```
 */
class SendEditorFragment : Fragment("Email Editor") {
  /* TODO:[SendEditorFragment::SendController] eventbus or scope for responsive bidirectional Trie data lookup */
  private val sendController: SendController by inject(FX.defaultScope)

  private val recipients: SimpleStringProperty = SimpleStringProperty()
  private val attachments: SimpleStringProperty = SimpleStringProperty()

  private val htmlEditor: HTMLEditor =
    htmleditor {
      prefHeight = 250.0
      vgrow = Priority.ALWAYS
      hgrow = Priority.ALWAYS
    }

  /* FIXME:[SendController::openSendEditorAndBindMessage] implement via scope, eventbus, or ok? */
  val sendMessageModel: SendMessageModel by param()

  override val root: Form = form {
    fieldset {
      field("Sender name:") {
        textfield(sendMessageModel.senderName)
      }

      field("Sender address:") {
        textfield(sendMessageModel.senderAddress)//.required()
      }

      field("Recipient name:") {
        textfield(sendMessageModel.recipientName)
      }

      /* TEST:[SendEditorFragment::recipient address] what if user removes/updates a field what happens in the textfield?? */
      field("Recipient address:") {
        textfield(recipients) {
          setOnKeyReleased {
            if (Character.isLetterOrDigit(it.character[0]) && sendController.containsAddress(it.text)) {
              listview(sendController.lookupAddressMatches(it.text)) {
                selectionModel.selectionMode = SelectionMode.MULTIPLE
                // selectionModel.selectedItems.foreach { str -> recipients + "$str; " }
                selectionModel.selectedItems.whileEach { str -> recipients.value + "$str; " }
                // selectionModel.selectedItems.foreach { str -> this@textfield.text += "$str; " }
              }
            }
          }
          // FIXME: validate that users independent input of multiple address ends with a semicolon or comma
          // required()
        }
      }

      field("Subject:") {
        textfield(sendMessageModel.subject)//.required()
      }

      /* FIXME:[SendEditorFragment::Attachment] throws error when user cancels the inherited platform ui box */
      field("Attachment") {
        button("Select F5") {
          action {
            val files: List<File> = chooseFile(
              filters = arrayOf(FileChooser.ExtensionFilter("All Files", "*.*")),
              mode = FileChooserMode.Multi
            )
            sendMessageModel.attachment.value + files
            attachments + files.joinToString(transform = { it.name })
            // attachments.value + files.joinToString(transform = { it.name })
          }
          shortcut("F5")
        }

        button("Remove F6") {
          action {
            sendMessageModel.rollback(sendMessageModel.attachment)
            attachments.value = ""
          }
          shortcut("F6")
        }
      }

      field("Attachments:") {
        textfield(attachments) {
          isEditable = false
        }
      }

      field {
        this.add(htmlEditor)
      }

      buttonbar {
        button("Send F9") {
          enableWhen(sendMessageModel.valid)
          action { commitMessageProperties() }
          shortcut("F9")
        }

        button("Clear F2") {
          action { htmlEditor.htmlText = "" }
          shortcut("F2")
        }

        button("Close ESC") {
          action { close() }
        }
      }
    }
  }

  override fun onUndock() {
    /* TEST:[SendEditorFragment::onUndock] sendMessageModel.rollback is it required to reset the model?? */
    // sendMessageModel.rollback()
    /* REMOVE:[SendEditorFragment] Console.showInfo */
    Console.showInfo("Address: ${sendMessageModel.recipientAddress.joinToString()}\nContent: ${sendMessageModel.content.valueSafe}")
  }

  /* TEST:[SendEditorFragment::onDock] pre-populate the users email address check and apply for user email alias */
  override fun onDock() {
    // sendMessageModel.validate(decorateErrors = false)
    sendMessageModel.senderAddress.value + sendController.checkHostAddress()
  }

  private fun checkEmailAddress(address: String): Boolean =
    Console.isSuccess("SendEditorFragment invalid format: $address") {
      InternetAddress(address).validate()
    }

  private fun commitMessageProperties() {
    val addressList: List<String> = recipients.valueSafe.replace(" ", "").split("[;,:]".toRegex())

    addressList.whileEach { address ->
      when {
        sendController.containsAddress(address) -> sendMessageModel.recipientAddress.add(InternetAddress(address))
        checkEmailAddress(address) -> sendMessageModel.recipientAddress.add(InternetAddress(address))
        else -> {
          sendMessageModel.rollback(sendMessageModel.recipientAddress)
          error("Recipient address error", "please check $address is correctly formatted")
        }
      }
    }

    sendMessageModel.content.value = htmlEditor.htmlText

    if (sendMessageModel.commit()) {
      sendController.sendMessage(sendMessageModel)
      close()
    } else {/* FIXME:[commitMessageProperties] refactor to help user identify the error in the ui on commit failure */
      error("Message Error", "Unable to commit message, please check all fields are correctly formatted")
    }
  }
}
