package view

import javafx.scene.layout.StackPane
import tornadofx.*

class CenterView : View() {
  private val sentTableView: SentTableView by inject()
  private val inboxTableView: InboxTableView by inject()

  override val root: StackPane = stackpane {
    add(sentTableView.root)
    add(inboxTableView.root)
  }
}
