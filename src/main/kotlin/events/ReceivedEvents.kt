package events

import model.ReceivedMessage
import tornadofx.*

class ReceivedEvent(val received: ReceivedMessage, val forward: Boolean) : FXEvent()

class ReceivedFormEvent(val received: ReceivedMessage) : FXEvent()
