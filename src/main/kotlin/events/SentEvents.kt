package events

import model.Sentbox
import tornadofx.*

class SentEvent(val sent: Sentbox) : FXEvent()

class SentViewEvent(val sent: Sentbox) : FXEvent()
