package io

import com.natpryce.Result
import com.natpryce.flatMap
import com.natpryce.map
import com.natpryce.onFailure
import com.natpryce.resultFrom
import com.natpryce.valueOrNull
import model.SendMessageModel
import java.io.File

class FileManager(private val model: SendMessageModel) {
  /* TODO:[FileManager::model] implement class and methods
       every mail message needs to be written to a temp file before sending,
       once a confirmed sent receipt given delete the temp file. Upon a sending
       error request user to save the temp file in local location or delete it.
       If a user saves a message need to be able to open it again in SendEditor. */
  private val current: String = System.currentTimeMillis().toString()
  private val fileName: String = "$current-mailer"
  private val extension: String = ".txt"
  private val fileResult: Result<File, Exception> = createFile(fileName)

  private fun saveFile(name: String): Result<Boolean, Exception> {
    return createFile(name)
      .flatMap { file: File ->
        copyTempToFile(file)
          .map { wd ->
            wd.exists() && wd.length() > 0L && deleteFile().onFailure { return it }
          }
      }
  }

  private fun createFile(name: String): Result<File, Exception> =
    resultFrom { createTempFile(prefix = name, suffix = extension) }

  private fun copyTempToFile(newFile: File): Result<File, Exception> =
    fileResult.map { file: File -> file.copyTo(newFile) }

  private fun writeFile(): Result<Unit, Exception> =
    fileResult.map { file: File ->
      file.writeText("${model.recipientAddress.value.joinToString()}\n\n${model.content.valueSafe}")
    }

  fun fileName(): String? = fileResult.valueOrNull()?.absolutePath

  fun deleteFile(): Result<Boolean, Exception> =
    fileResult.map { it.delete() }
}
