package io

import io.DurationTimer.start
import io.DurationTimer.stopMilliseconds
import io.DurationTimer.stopSeconds
import java.lang.reflect.Method
import kotlin.time.milliseconds
import kotlin.time.minutes
import kotlin.time.seconds

/**
 * [DurationTimer] a stopwatch. The [start] and [stopMilliseconds],
 * [stopSeconds] methods log with [Console.showInfo].
 */
object DurationTimer {
  private var started: Long = 0L

  fun start(): Unit {
    check(started == 0L) { "Timer already running $started(ms), requires a stop call on previous start." }
    started = System.currentTimeMillis()
  }

  fun stopMilliseconds(): Unit {
    Console.showInfo(
      "Duration [ms]: ${stopped()}"
    )
  }

  fun <T> stopMilliseconds(clazz: Class<T>): Unit {
    Console.showInfo(
      "${clazz.simpleName} duration [ms]: ${stopped()}"
    )
  }

  fun <T> stopMilliseconds(clazz: Class<T>, method: Method): Unit {
    Console.showInfo(
      "${clazz.simpleName}.${method.name} duration [ms]: ${stopped()}"
    )
  }

  fun stopSeconds(): Unit {
    Console.showInfo(
      "Duration [sec]: ${stopped().seconds}"
    )
  }

  fun <T> stopSeconds(clazz: Class<T>): Unit {
    Console.showInfo(
      "${clazz.simpleName} duration [sec]: ${stopped().seconds}"
    )
  }

  fun <T> stopSeconds(clazz: Class<T>, method: Method): Unit {
    Console.showInfo(
      "${clazz.simpleName}.${method.name} duration [sec]: ${stopped().seconds}"
    )
  }

  fun stopMinutes(): Unit {
    Console.showInfo(
      "Duration [min]: ${stopped().minutes}"
    )
  }

  fun <T> stopMinutes(clazz: Class<T>): Unit {
    Console.showInfo(
      "${clazz.simpleName} duration [min]: ${stopped().minutes}"
    )
  }

  fun <T> stopMinutes(clazz: Class<T>, method: Method): Unit {
    Console.showInfo(
      "${clazz.simpleName}.${method.name} duration [min]: ${stopped().minutes}"
    )
  }

  fun stopAllDurations() {
    val stopped: Long = stopped()
    Console.showInfo(
      "Duration in [min]: ${stopped.minutes}, [sec]: ${stopped.seconds}, [ms]: ${stopped.milliseconds}}"
    )
  }

  fun <T> stopAllDurations(clazz: Class<T>) {
    val stopped: Long = stopped()
    Console.showInfo(
      "${clazz.simpleName} duration in [min]: ${stopped.minutes}, [sec]: ${stopped.seconds}, [ms]: ${stopped.milliseconds}}"
    )
  }

  fun <T> stopAllDurations(clazz: Class<T>, method: Method) {
    val stopped: Long = stopped()
    Console.showInfo(
      "${clazz.simpleName}.${method.name} duration in [min]: ${stopped.minutes}, [sec]: ${stopped.seconds}, [ms]: ${stopped.milliseconds}}"
    )
  }

  private fun stopped(): Long {
    check(started > 0L) { "Timer not running $started(ms), requires a start call." }
    return (System.currentTimeMillis() - started).also { reset() }
  }

  private fun reset() {
    started = 0L
  }
}
