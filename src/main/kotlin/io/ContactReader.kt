package io

import com.natpryce.Result
import com.natpryce.resultFrom
import model.Trie
import model.collections
import model.contains
import model.insert
import prop.MailerContactProperty
import util.whileEach
import java.io.File
import java.nio.file.Path
import java.nio.file.Paths
import javax.mail.internet.InternetAddress

object ContactReader {
  /* TODO:[ContactLoader::writer] save/append/write email address to mailercontact.properties file from a sent/received email
       autocomplete implementation, function needs to parse the properties...to create InternetAddress object...
       to pass into Trie data structure.
       */
  private const val CONFIGFILENAME: String = "mailercontact.properties"
  private val SEPERATOR: String = File.separator
  private val CONFIGPATH: String = "src${SEPERATOR}main${SEPERATOR}resources$SEPERATOR"
  private val configPath: Path = Paths.get("$CONFIGPATH$CONFIGFILENAME")

  private val checkContactAddress: String.() -> Boolean =
    { startsWith(MailerContactProperty.CONTACT_ADDRESS_string.key()) }
  private val checkContactList: String.() -> Boolean =
    { startsWith(MailerContactProperty.CONTACT_LIST_string.key()) }
  private val checkContactGroup: String.() -> Boolean =
    { startsWith(MailerContactProperty.CONTACT_GROUP_string.key()) }

  private val isNotCommentedOut: String.() -> Boolean =
    { startsWith("#").not() }
  private val isContactAddress: String.() -> Boolean =
    { isNotEmpty() && isNotCommentedOut() && checkContactAddress() }

  private val parseContactAddress: String.() -> List<String> =
    {
      substringAfter('=')
        .replace(" ", "")
        .split(';', ':', ',')
    }

  private val prettyPrintContacts: List<Char>.() -> String =
    { this.joinToString().replace("[", "").replace("]", "\n") }

  private val _trie: Lazy<Trie<Char>> = lazy { Trie<Char>() }

  val trieCollections: String.() -> List<String>
    get() = { _trie.value.collections(this) }

  val trieContains: String.() -> Boolean
    get() = { _trie.value.contains(this) }

  val triePrintContacts: () -> String
    get() = { _trie.value.lists.flatten().prettyPrintContacts() }

  /* TODO:[ContactLoader::invoke] implement contact lists and groups */
  private fun loadContacts(path: String) {
    /* FIXME:[ContactReader::loadContacts] horrible algorithm */
    File(path).forEachLine {
      if (it.isContactAddress()) {
        it.parseContactAddress().whileEach { address ->
          val success: Boolean =
            Console.isSuccess("ContactReader invalid format: $address")
            { InternetAddress(address).validate() }
          if (success) _trie.value.insert(address)
        }
      }
    }
  }

  /* TODO:[ContactReader::invoke] option to select path to mailer.properties file */
  operator fun invoke(path: String): Result<Unit, Exception> =
    resultFrom { loadContacts(path) }

  operator fun invoke(): Result<Unit, Exception> =
    resultFrom { loadContacts(configPath.toString()) }

  /* NOTE:[ContactLoader::Arkenv] look here for parsers https://github.com/aPureBase/arkenv/tree/master/arkenv/src/main/kotlin/com/apurebase/arkenv
       elementlist kotlin docs, p.69
       joy of kotlin https://github.com/pysaumont/fpinkotlin/blob/master/fpinkotlin-parent/fpinkotlin-commonproblems/src/main/resources/config.properties
       kotlin.io https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.io/java.io.-file/index.html
       rosettacode http://rosettacode.org/wiki/Read_a_configuration_file#Kotlin */
  /* TEST:[ContactLoader::trie] split/match/pick on lists, groups, address, names */
}
