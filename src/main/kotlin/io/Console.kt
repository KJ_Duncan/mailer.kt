package io

import io.Console.isFailure
import io.Console.isSuccess
import io.Console.outputException
import io.Console.print
import io.Console.println
import io.Console.showDebug
import io.Console.showError
import io.Console.showInfo
import io.Console.showThrow
import io.Console.showWarn
import mu.KLogger
import mu.KotlinLogging

typealias Log = (String) -> Unit
typealias LogThrow = (Throwable) -> Unit

/**
 * Console.logger wraps the mu.KotlinLogging.logger and is accessible via;
 * showDebug, showInfo, showWarn, showError, and showThrow property members.
 *
 * [Console UML](/bashit.kak/src/main/resources/img/Console.png)
 *
 * Console.isSuccess is a function that logs on error and returns a boolean value.
 * It takes a user defined message on failure and wraps a function with no
 * return type inside a try catch block. isSuccess returns true on success
 * and false on failure.
 *
 * Console.isFailure is the compliment of Console.isSuccess.
 *
 * Console.outputException throws an exception to stdout for ease of debugging a failed process.
 *
 * Console.println and Console.print values implement kotlin.io.println and kotlin.io.print.
 *
 * The documentation strings for DEBUG, INFO, WARN, ERROR, and FATAL are directly quoted from source:
 * @author Hyperskill & Jetbrains 2019, Theory: Introduction to logging, Log levels,
 *         viewed on 18 Nov 2019, https://hyperskill.org/learn/step/5538
 *
 * @author MicroUtils 2019, kotlin-logging, github.com, viewed on 27 Nov 2019,
 *         https://github.com/MicroUtils/kotlin-logging
 *
 * @author QOS.ch (Quality Open Software) 2019, Simple Logging Facade for Java (SLF4J),
 *         slf4j-simple, github.com, viewed on 27 Nov 2019,
 *         https://github.com/qos-ch/slf4j/tree/master/slf4j-simple
 *
 * @author Saumont, P-Y 2019, Fully functional input/output, The Joy of Kotlin,
 *         chpt 12, p.341, ex 12.5, Manning Publications,
 *         https://www.manning.com/books/the-joy-of-kotlin
 *
 * @author Syse, E 2017, KDBC: SQL DSL for Kotlin, kdbc.kt, github.com, viewed on 02 Jan 2020,
 *         https://github.com/edvin/kdbc/blob/master/src/main/kotlin/kdbc/kdbc.kt#L26
 *
 * @author JetBrains 2019, Kotlinx Coroutines, TestUtil.kt, github.com, viewed on 15 Jan 2020,
 *         https://github.com/Kotlin/kotlinx.coroutines/blob/master/kotlinx-coroutines-core/jvm/test/guide/test/TestUtil.kt#L16
 *
 *  @property showDebug [Console.showDebug]
 *  @property showInfo [Console.showInfo]
 *  @property showWarn [Console.showWarn]
 *  @property showError [Console.showError]
 *  @property showThrow [Console.showThrow]
 *  @property isSuccess [Console.isSuccess]
 *  @property isFailure [Console.isFailure]
 *  @property outputException [Console.outputException]
 *  @property print [kotlin.io.print]
 *  @property println [kotlin.io.println]
 */
object Console {
  private val logger: KLogger = KotlinLogging.logger {}

  /** [DEBUG][showDebug] diagnose applications by sysadmin, testers, for values and variables */
  @JvmField
  val showDebug: Log = { logger.debug { it } }

  /** [INFO][showInfo] service start, stop, configurations, assumptions */
  @JvmField
  val showInfo: Log = { logger.info { it } }

  /** [WARN][showWarn] first level of application failure when repeated attempts to access a resource, missing secondary data, switching from primary to backup server */
  @JvmField
  val showWarn: Log = { logger.warn { it } }

  /** [ERROR][showError] second level application failure has effected the result of an operation but has not terminated the program. */
  @JvmField
  val showError: Log = { logger.error { it } }

  /** [FATAL][showThrow] third level of application failures which has resulted in the termination of the program. */
  @JvmField
  val showThrow: LogThrow = { logger.throwing(it) }

  /**
   * isSuccess is a try catch wrapper that returns true on success and logs a failure with return of false.
   *
   * @param msg is the user defined log message
   * @param op the function to try with no return type
   * @return [kotlin.Boolean] true on success and false on failure
   */
  inline fun isSuccess(msg: String, op: () -> Unit): Boolean =
    try {
      op()
      true
    } catch (ex: Throwable) {
      showWarn("$msg\n${ex.cause}\n${ex.message}")
      false
    }

  /**
   * isFailure is a try catch wrapper that returns false on success and logs a failure with return of true.
   *
   * @param msg is the user defined log message
   * @param op the function to try with no return type
   * @return [kotlin.Boolean] true on success and false on failure
   */
  inline fun isFailure(msg: String, op: () -> Unit): Boolean =
    try {
      op()
      false
    } catch (ex: Throwable) {
      showWarn("$msg\n${ex.cause}\n${ex.message}")
      true
    }

  /**
   * outputException throws an exception to stdout for ease of debugging a failed process.
   *
   * @param op the function to try with a generic return type
   * @return R is a generic type of the success
   * @throws [mu.KLogger.throwing] with [kotlin.printStackTrace] to console
   */
  inline fun <R> outputException(op: () -> R): R =
    try {
      op()
    } catch (ex: Throwable) {
      showThrow(ex)
      kotlin.system.exitProcess(2)
    }

  val print: (Any) -> Unit = { kotlin.io.print(it.toString()) }

  val println: (Any) -> Unit = { kotlin.io.println(it.toString()) }
}
