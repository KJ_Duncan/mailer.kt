package util

/**
 * Implements a [whileEach] iteration on an ArrayList aka listOf() using a 'while' loop.
 *
 * @author Kucherenko, I 2018, 'Enhancing the Performance of Collections',
 *         Mastering High Performance with Kotlin, chpt 5, p.141,
 *         Packet Publishing, Birmingham UK, www.packtpub.com.
 *
 * @author OpenJDK, 'Java Microbenchmark Harness (JMH)', code-tools, jmh,
 *         viewed on 01 April 2020, https://openjdk.java.net/projects/code-tools/jmh/
 *
 * @author JetBrains 2019, jmh-kotlin, kotlin-benchmarks, JetBrains GitHub,
 *         viewed 28 April 2020, https://github.com/Kotlin/kotlin-benchmarks 
 */
/* TEST:[List<T>::whileEach] reified Type and call site crossinline function, research runtime and byte code */
inline fun <reified T> List<T>.whileEach(crossinline invoke: (T) -> Unit): Unit {
  val size = size
  var i = 0
  while (i < size) {
    invoke(get(i))
    i++
  }
}

/*
QUESTION: How many is too many for a lightweight mail application? Out of memory stack overflow!

This will be used for the TRIE lookup, potentially large contact list (University's database).
And to traverse all INBOX* messages, potentially large list (Unbounded email storage).
Only two (known) bottlenecks Inbox traversal and Trie lookup. Check Sequence<T> for lazy eval?

Final benchmark will have sample size 15 and a range 10^1, 10^6.
(an exercise into kotlin data structures and jmh benchmarking)

$ java -jar target/benchmarks.jar MyWhileEachLoopBenchmark -wi 3 -i 5 -f 1

Trie requires more testing on iteration and efficiency. 
Mailbox uses filter function: T<A> -> List<A>

Analysis of Algorithms; predict performance, compare algorithms, provide guarantees. 
[Avoids] performance bugs (Sedgewick & Wayne 2020, s1.4 p.5).

Benchmark                              (size)  Mode  Cnt   Score   Error  Units
arrayForEachLoop                      1000000  avgt    5   3.898 ± 0.056  ms/op
arrayWhileEachLoop                    1000000  avgt    5   4.806 ± 0.708  ms/op
arrayWhileEachCrossinlineLoop         1000000  avgt    5   3.904 ± 0.031  ms/op

listForEachLoop                       1000000  avgt    5   9.515 ± 0.174  ms/op
listWhileEachLoop                     1000000  avgt    5   9.289 ± 2.703  ms/op
listWhileEachCrossinlineLoop          1000000  avgt    5   8.697 ± 0.716  ms/op

Do not assume the numbers tell you what you want them to tell you (JMH).

inline fun <T> List<T>.whileEach(invoke: (T) -> Unit): Unit {
  val size = size
  var i = 0
  while (i < size) {
    invoke(get(i))
    i++
  }
}

inline fun <Key> Iterable<Key>.whileEach(invoke: (Key) -> Unit): Unit {
  val iterator = iterator()
  while (iterator.hasNext()) {
    val next = iterator.next()
    invoke(next)
  }
}

inline fun IntRange.whileEach(invoke: (Int) -> Unit): Unit {
  val endInclusive = endInclusive
  var i = start
  while (i <= endInclusive) {
    invoke(i)
    i++
  }
}
 */
