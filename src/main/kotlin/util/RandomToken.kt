package util


/* FIXME: Set<RandomToken.next()>: CRUD (create, read, update, delete)
     @see: https://github.com/plantuml/plantuml/blob/master/src/net/sourceforge/plantuml/utils/Cypher.java
           https://github.com/ktorio/ktor/blob/master/ktor-client/ktor-client-features/ktor-client-auth/common/src/io/ktor/client/features/auth/providers/DigestAuthProvider.kt
           https://developer.android.com/guide/topics/security/cryptography#kotlin
           https://github.com/etiennestuder/gradle-credentials-plugin/blob/master/src/main/groovy/nu/studer/gradle/credentials/domain/Encryption.java */


/** NOTE: early days of; research, reading, reminding, and rewiring...aka learning / prototyping
 *  @see https://www.usc.edu.au/current-students/student-support/academic-and-study-support/online-study-resources/academic-integrity
 *       https://bitbucket.org/KJ_Duncan/jse-security-guide/src/master/
 *
 * Random token sha-256 hash generator for user to application, application to user authentication.
 *
 * @author JetBrains 2020, RandomCode.kt, Kotless-Kotlin Serverless Framework,
 *         JetBrains GitHub Kotless, viewed 26 April 2020,
 *         https://github.com/JetBrains/kotless/blob/master/examples/kotless-shortener/src/main/kotlin/io/kotless/examples/utils/RandomCode.kt
 *
 * @author Oracle 2019, Java Platform, Standard Edition Security Developer’s Guide, Release 13,
 *         viewed on 28 April 2020, https://docs.oracle.com/en/java/javase/13/security/security-developer-guide.pdf
 * 
 * @author Mayoral, F 2013, Instant Java Password and Authentication Security, Packt Publishing, 
 *         viewed on 28 May 2020, https://www.packtpub.com/application-development/instant-java-password-and-authentication-security-instant
 */
internal object RandomToken {
  private val rnd = java.security.SecureRandom()

  /** It is highly recommended to use default 36 radix **/
  private fun next(radix: Int = 36): String {
    return java.math.BigInteger(128, rnd).toString(radix)
  }

  /** In Kotlin, an outer class does not see private members of its inner classes. */
  private object MDHashToken {
    private val printToken: String.() -> Unit = { println(this.hash()) }

    /*
     * To sign up or change a password, follow the given steps: 
     *   1. Generate a random salt value. 
     *   2. Create a MessageDigester with an algorithm you prefer.
     *   3. Add the salt to the MessageDigester.
     *   4. Digest the password with the MessageDigester.
     *   5. Get the hash from the digest.
     *   6. Save the generated salt and the hashed password. In case of sign up, we need to save the username.
     */
    private fun createSalt(random: SecureRandom = rnd): ByteArray {
      // storage 32*8=256-bits for SHA-256
      val salt = ByteArray(32)
      random.nextBytes(salt)
      return salt
    }

    // https://github.com/corda/corda/blob/release/os/4.6/core/src/main/kotlin/net/corda/core/crypto/SecureHash.kt
    private fun CharArray.hash(salt: ByteArray = createSalt()): ByteArray {
      val sha256 = java.security.MessageDigest.getInstance("SHA-256")
      sha256.update(salt)
      val output = sha256.digest(this.toByteArray(Charsets.UTF_8))
      sha256.reset()
      Arrays.fill(this, ' ')
      return output
    }
    operator fun invoke(str: String) { str.printToken() }
  }

  private object PBEHashToken {
    private val iterations = 1000
    
    /*
     * Password based encryption (PBE)
     *
     * To create the first hash (Sign up), follow the given steps: 
     *   1. Get the password as a char array. 
     *   2. Create a salt value.
     *   3. Create a password based encryption key spec.
     *   4. Create a key factory.
     *   5. Generate the hash.
     *   6. Add the iterations and the original salt to your hash.
     */
    private fun createSalt(random: SecureRandom = rnd): ByteArray {
      // storage 32*8=256-bits for SHA-256
      val salt = ByteArray(32)
      random.nextBytes(salt)
      return salt
    }

    private fun CharArray.hash(salt: ByteArray = createSalt()): String {
      TODO("PBEKeySpec, SecretKeyFactory")
      return "$iterations:${toHex(createSalt())}:${toHex(hash)}"
    }

    private fun toHex(): Unit {}

    private fun fromHex(): Unit {}

    private fun slowEquals(): Unit {}

    operator fun invoke(text: CharArray) {}
  }

  /*
   * To compare hashes (Authentication), follow the given steps: 
   *   1. Get the password as a char array.
   *   2. Get the stored password with its iterations and salt.
   *   3. Create a password-based encryption key spec.
   *   4. Create a key factory.
   *   5. Generate the hash formatted with the salt and the iterations.
   *   6. Compare the generated hash with the stored one.
   */
  private fun isValidUser(): Boolean {
    TODO("Compare generated and stored hashes")
    return slowEquals(hash, testHash)
  }
  
  operator fun invoke(text: CharArray) { MDHashToken(text) }
}

// fun main() { RandomToken("ramdomise a list of text for user choice???") }
/* https://github.com/corda/corda/blob/release/os/4.6/core/src/main/kotlin/net/corda/core/contracts/Structures.kt#L324
init {
  require(bytes.size == 32) { "Privacy salt should be 32 bytes." }
  require(bytes.any { it != 0.toByte() }) { "Privacy salt should not be all zeros." }
}
*/
