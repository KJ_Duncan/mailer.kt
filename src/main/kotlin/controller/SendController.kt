package controller

import events.ReceivedEvent
import events.SendEvent
import events.SentEvent
import io.Console
import io.ContactReader
import io.FileManager
import javafx.collections.ObservableList
import javafx.geometry.Pos
import javafx.stage.StageStyle
import model.ReceivedMessage
import model.SendMessageModel
import model.Sentbox
import org.simplejavamail.api.email.Email
import org.simplejavamail.api.mailer.Mailer
import prop.JakartaMailProperty
import prop.PropertiesWrapper
import prop.UserSettingProperty
import tornadofx.*
import tornadofx.controlsfx.errorNotification
import tornadofx.controlsfx.infoNotification
import view.SendEditorFragment

class SendController : Controller() {
  private val smtpController: SMTPController by inject(FX.defaultScope)
  private val sendMessageModel: SendMessageModel by inject(FX.defaultScope)

  fun containsAddress(text: String): Boolean =
    ContactReader.trieContains(text)

  fun lookupAddressMatches(text: String): ObservableList<String> =
    ContactReader.trieCollections(text).toObservable()

  /* TEST:[SendEditorFragment::onDock] check if alias email has been set */
  fun checkHostAddress(): String =
    when (val alias: String = PropertiesWrapper.props[UserSettingProperty.ALIAS_EMAIL_string.key()] as String) {
      "" -> PropertiesWrapper.props[JakartaMailProperty.MAIL_IMAPS_USER_string.key()] as String  // "some@address"
      else -> alias.toString()
    }

  fun sendMessage(messageModel: SendMessageModel) {
    /* TEST:[sendMessage] sendMessageModel.recipientAddress.value is holding the correct information List<String> */
    Console.showInfo("Content: ${this.sendMessageModel.content.valueSafe},\nAddress: ${this.sendMessageModel.recipientAddress.value}")

    val fileManager = FileManager(messageModel)

    /* TODO:[SendController] runAsync with TaskStatus */
    kotlin.runCatching {
      runAsync {
        val mailer: Mailer = smtpController.smtpOutlookMailBuilder()
        val email: Email = smtpController.sendMessageBuilder(messageModel)
        mailer.sendMail(email)
        infoNotification("Email Receipt", email.id, Pos.TOP_RIGHT)
      }
      fileManager.deleteFile()
    }.getOrElse {
      errorNotification(
        "Send Mail Error",
        "Message content saved to: ${fileManager.fileName()}$\n\n{it.cause}\n${it.message}",
        Pos.TOP_RIGHT
      )
    }
  }

  private fun sentMessageTemplateBuilder(sent: Sentbox) {
    sendMessageModel.subject.set("Fwd: ${sent.subject}")
    sendMessageModel.content.set(messageTemplate(sent))
    openSendEditorAndBindMessage()
  }

  private fun receivedMessageTemplateBuilder(receivedMessage: ReceivedMessage, forward: Boolean) {
    if (!forward) sendMessageModel.recipientAddress.set(receivedMessage.from.asObservable())
    sendMessageModel.subject.set(messageSubjectTemplate(receivedMessage, forward))
    sendMessageModel.content.set(messageTemplate(receivedMessage))
    openSendEditorAndBindMessage()
  }

  /* TODO:[SendEditorFragment::SendController] eventbus or scope for responsive bidirectional Trie data lookup */
  private fun openSendEditorAndBindMessage() {
    find<SendEditorFragment>(mapOf(SendEditorFragment::sendMessageModel to sendMessageModel))
      .openModal(stageStyle = StageStyle.UTILITY)
  }

  private fun messageSubjectTemplate(receivedMessage: ReceivedMessage, forward: Boolean): String =
    when {
      forward -> "Fwd: ${receivedMessage.subject}"
      else    -> "Re: ${receivedMessage.subject}"
    }

  private fun messageTemplate(receivedMessage: ReceivedMessage): String =
    "Date: ${receivedMessage.date}\nFrom: ${receivedMessage.from.joinToString()}\nSubject: ${receivedMessage.subject}\nMessage: ${receivedMessage.content}\n-------------------------------------------\n\n"

  private fun messageTemplate(sent: Sentbox): String =
    "Date: ${sent.date}\nFrom: ${sent.receiver.joinToString()}\nSubject: ${sent.subject}\nMessage: ${sent.content}\n-------------------------------------------\n\n"

  /* NOTE:[SendController::init] controllers are lazily loaded, so if nothing references your controller,
      the subscriptions will never be registered in the first place. */
  init {
    subscribe<SentEvent> {
      sentMessageTemplateBuilder(it.sent)
    }

    subscribe<ReceivedEvent> {
      receivedMessageTemplateBuilder(it.received, it.forward)
    }

    subscribe<SendEvent> {
      openSendEditorAndBindMessage()
    }
  }
}
