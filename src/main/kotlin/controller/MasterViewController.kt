package controller

import io.Console
import javafx.scene.Node
import javafx.util.Duration
import tornadofx.*
import view.InboxTableView
import view.MasterView
import view.SentTableView
import java.util.Timer
import kotlin.concurrent.timerTask

class MasterViewController : Controller() {

  private val masterView: MasterView by inject(FX.defaultScope)
  private val inboxTableView: InboxTableView by inject(FX.defaultScope)
  private val sentTableView: SentTableView by inject(FX.defaultScope)
  private val imapController: IMAPController by inject(FX.defaultScope)

  fun scheduleInbox(duration: Duration): Unit = Timer().schedule(timerTask {
    imapController.imapMailboxBuilder()
    showCenterView()
  }, duration.toMillis().toLong())

  /* FIXME:[MasterViewController::checkInboxView] correct logging and ui error notification */
  fun checkInboxView() {
    if (imapController.imapMailboxBuilder()) updateChildNode(inboxTableView.root)
    else Console.showInfo("imapMailboxBuilder failed.")
  }

  fun checkSentView(): Unit = updateChildNode(sentTableView.root)

  private fun updateChildNode(node: Node?): Unit = when (node) {
    sentTableView.root -> {
      inboxTableView.root.hide()
      sentTableView.root.show()
      showCenterView()
    }
    inboxTableView.root -> {
      sentTableView.root.hide()
      inboxTableView.root.show()
      showCenterView()
    }
    else -> {
      sentTableView.root.hide()
      inboxTableView.root.hide()
      hideCenterView()
    }
  }

  /* TODO:[CenterView] hide stackpane and reopen when inbox timer has run */
  private fun hideCenterView() {
    if (isCenterViewShowing()) masterView.root.center.hide()
  }

  private fun showCenterView() {
    if (!isCenterViewShowing()) masterView.root.center.show()
  }

  private fun isCenterViewShowing(): Boolean = masterView.root.center.isVisible
}
