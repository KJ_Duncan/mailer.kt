package controller

import StoreBuilder
import io.Console
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import model.Inbox
import model.ReceivedMessage
import model.Sentbox
import org.jsoup.Jsoup
import org.jsoup.safety.Whitelist
import prop.PropertiesWrapper
import prop.RenderHTML
import prop.RenderHTML.*
import prop.UserSettingProperty.RENDER_HTML_api
import tornadofx.*
import util.whileEach
import javax.mail.Flags
import javax.mail.Folder
import javax.mail.Message
import javax.mail.event.MessageCountAdapter
import javax.mail.event.MessageCountEvent
import javax.mail.internet.MimeMultipart

class IMAPController : Controller() {

  private val inbox: String = "INBOX*"
  private val sent: String = "Sent*"

  // could simplify by flatten/divide INBOX* into each their own ObservableList 
  // using kotlin asynchronous flows which are cold streams similar to sequences
  // then concatenation for new GUI inbox ObservableList.
  // @see https://kotlinlang.org/docs/reference/coroutines/flow.html
  private val inboxObservableList: ObservableList<Inbox> =
    FXCollections.observableArrayList<Inbox>()
  private val sentBoxObservableList: ObservableList<Sentbox> =
    FXCollections.observableArrayList<Sentbox>()

  private val today: Long = System.currentTimeMillis()
  private var mailboxChecked: Long = 0L

  /* FIXME:[IMAPController::StoreBuilder] implement via an eventbus? */
  private lateinit var mStore: StoreBuilder

  fun setStore(store: StoreBuilder): Unit { mStore = store }

  fun imapInboxObservable(): ObservableList<Inbox> = inboxObservableList

  fun imapSentBoxObservable(): ObservableList<Sentbox> = sentBoxObservableList

  fun imapConnected(): Boolean = mStore.isConnected

  fun imapDisconnect(): Boolean = when {
    imapConnected() -> { mStore.close(); imapConnected() }
    else -> false
  }

  /* TODO:[ProgressIndicator] on runAsync: TaskStatus object via css stylesheet or just the graphic? */
  fun imapMailboxBuilder(): Boolean =
    when (mailboxChecked) {
      0L -> {
        Console.showInfo("Running imapInboxBuilder and imapSentBuilder.")
        imapInboxBuilder() //&& imapSentBuilder()
      }
      else -> {
        Console.showInfo("Running updateInbox and updateSentbox.")
        updateInbox() //&& updateSentbox()
      }
    }

  /* NOTE: filter is a transformation function T<A> -> List<A> 
       see https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.collections/filter.html */
  // isSeen(it) only point of difference
  // Folder -> Filter -> ForEach -> Open -> Filter -> ForEach -> Close
  private fun imapInboxBuilder(): Boolean =
    kotlin.runCatching {
      runAsync {
        val received: MutableList<ReceivedMessage> = mutableListOf<ReceivedMessage>()
        inboxFolders()
          .filter { hasInboxUnreadMessages(it) }
          .whileEach { inbox: Folder ->
            inbox.open(1)
            inbox.messages.filter { isSeen(it) }
              .whileEach { buildReceived(it, received) }  // quadratic time is too slow for large algorithms (they dont scale)
            buildInbox(inbox, received)
            received.clear()
            inbox.close()
            Console.showInfo("inside imapInboxBuilder")
          }
      } ui {
        mailboxChecked = System.currentTimeMillis()  // check mail boxes from/against this time value
        Console.showInfo("imapInboxBuilder checked: $mailboxChecked")
      }
    }.isSuccess

  private fun imapSentBuilder(): Boolean =
    kotlin.runCatching {
      Console.showInfo("Running imapSentBuilder")
      runAsync {
        sentFolder().apply {
          open(1)
          messages.filter { hasSentMessages(it) }
            .whileEach { buildSentBox(it) }
        }.close()
      }
    }.isSuccess

  // anyNewInboxMessages(it) only point of difference
  // Folder -> Filter -> ForEach -> Open -> Filter -> ForEach -> Close
  private fun updateInbox(): Boolean =
    kotlin.runCatching {
      runAsync {
        val received: MutableList<ReceivedMessage> = mutableListOf<ReceivedMessage>()
        inboxFolders()
          .filter { hasInboxUnreadMessages(it) }
          .whileEach { inbox: Folder ->
            inbox.open(1)
            inbox.messages.filter { anyNewInboxMessages(it) }
              .whileEach { buildReceived(it, received) }  // quadratic time is too slow for large algorithms (they dont scale)
            /* FIXME:[IMAPController::updateInbox] Reset inbox counts to zero. Filter and update inboxes.
                if inbox in inboxObservableList append new mail to correct inbox via folderName,
                else create a new inbox and append to inboxObservableList. */
            buildInbox(inbox, received)
            received.clear()
            inbox.close()
          }
      } ui {
        mailboxChecked = System.currentTimeMillis()
        Console.showInfo("updateInbox checked: $mailboxChecked")
      }
    }.isSuccess

  private fun updateSentbox(): Boolean =
    kotlin.runCatching {
      Console.showInfo("updateSentbox running")
      runAsync {
        sentFolder().apply {
          open(1)  // 1 = read, 2 = write
          messages.filter { anyNewSentMessages(it) }
            .whileEach { buildSentBox(it) }
        }.close()
      }
    }.isSuccess

  private fun buildInbox(inbox: Folder, received: MutableList<ReceivedMessage>): Boolean =
    inboxObservableList.add(
      Inbox(
        folderName = inbox.fullName,
        unread = inbox.unreadMessageCount,
        messages = received.toList()
      )
    )

  private fun buildReceived(message: Message, received: MutableList<ReceivedMessage>): Boolean =
    received.add(
      ReceivedMessage(
        date = message.sentDate,
        from = message.from.toList(),
        subject = message.subject,
        content = checkPropsAndRenderHtml(imapMessageToString(message))
      )
    )

  private fun buildSentBox(message: Message): Boolean =
    sentBoxObservableList.add(
      Sentbox(
        date = message.sentDate,
        receiver = message.allRecipients.toList(),
        subject = message.subject,
        content = checkPropsAndRenderHtml(imapMessageToString(message))
      )
    )

  /**
   * Converts the given JavaMail message to a String body. Can return null.
   *
   * @author Apache Software Foundation, Camel 2020, 'MailConverters,java',
   *         package org.apache.camel.component.mail, viewed on 01 April 2020,
   *         https://www.programcreek.com/java-api-examples/?code=HydAu/Camel/Camel-master/components/camel-mail/src/main/java/org/apache/camel/component/mail/MailConverters.java
   */
  private fun imapMessageToString(message: Message): String =
    kotlin.runCatching {
      val sb = StringBuilder()

      when (val content: Any = message.content) {
        is MimeMultipart -> {
          val multipart: MimeMultipart = content
          if (multipart.count > 0) {
            (0 until multipart.count).forEach { i: Int ->
              sb.append(multipart.getBodyPart(i).content.toString())
            }
          }
        }
        else -> sb.append(content.toString())
      }
      return sb.toString()

    }.getOrDefault("N/A")

  private fun checkPropsAndRenderHtml(html: String): String =
    when (PropertiesWrapper.props[RENDER_HTML_api.key()] as RenderHTML) {
      Plain   -> cleanMessageBodyPlainText(html)
      Format  -> cleanMessageBodyFormatText(html)
      Images  -> cleanMessageBodyFormatWithImages(html)
    }

  /* NOTE:[Jsoup::cleanMessageHtml] does not clean the <head>content</head> */
  private fun cleanMessageBodyPlainText(html: String): String =
    Jsoup.clean(html, Whitelist.simpleText())

  private fun cleanMessageBodyFormatText(html: String): String =
    Jsoup.clean(html, Whitelist.basic())

  private fun cleanMessageBodyFormatWithImages(html: String): String =
    Jsoup.clean(html, Whitelist.basicWithImages())

  private fun isSeen(message: Message): Boolean =
    Flags.Flag.SEEN !in message.flags

  private fun hasInboxUnreadMessages(folder: Folder): Boolean =
    folder.type != 0 && folder.messageCount != 0 && folder.unreadMessageCount != 0

  private fun hasSentMessages(message: Message): Boolean =
    message.sentDate.time >= today

  private fun anyNewInboxMessages(message: Message): Boolean =
    message.receivedDate.time >= mailboxChecked && isSeen(message)

  private fun anyNewSentMessages(message: Message): Boolean =
    message.sentDate.time >= mailboxChecked

  private fun inboxFolders(): Array<Folder> =
    mStore.defaultFolder.list(inbox)

  private fun sentFolder(): Folder =
    mStore.getStoreFolder(sent)

  /* TEST:[IMAPController::addInboxListener] problems:
       does folder needs to remain open for applications lifecycle?
       no rechecks on auth folder -> application -> user -> application -> folder
       convenience over supervision == poor quality assurance */
  private fun addInboxListener(folder: Folder) {
    TODO()
    folder.addMessageCountListener(object : MessageCountAdapter() {
      override fun messagesAdded(e: MessageCountEvent) {
        // e.type.ADDED = 1, REMOVED = 2
        val type = e.type
      }
    })
  }
}
