package controller

import StoreBuilder
import events.Logout
import javafx.application.Platform
import javafx.geometry.Pos
import tornadofx.*
import tornadofx.controlsfx.errorNotification
import view.LoginView
import view.MasterView
import java.util.Arrays

/* NOTE: Controller: subscriptions are always active until you call unsubscribe and controllers load lazily. */
class LoginController : Controller() {
  private val loginView: LoginView by inject(FX.defaultScope)
  private val masterView: MasterView by inject(FX.defaultScope)

  private fun showLoginScreen() {
    masterView.replaceWith(loginView, sizeToScene = true, centerOnScreen = true)
  }

  private fun showMasterView() {
    loginView.replaceWith(masterView, sizeToScene = true, centerOnScreen = true)
  }

  /* TEST:[LoginController::tryLogin] implement via an eventbus, controllers are lazily loaded so may be a poor option here. */
  fun tryLogin(username: String, password: CharArray, host: String) {
    val mStore: StoreBuilder = StoreBuilder.createStoreBuilder(username, password, host)
    // find<SMTPController> { setSession(mSession) }.smtpConnected()
    if (find<IMAPController> { setStore(mStore) }.imapConnected()) showMasterView()
    else {
      /* NOTE: will be delegated to terminal UI for login < 3 attempts.
               The below error message is fine to adapt as no details leaked:
               how, when and why attempts failed (https://cwe.mitre.org/data/definitions/200.html).
               No final decisions will be made on login before completion of report:
               @see https://bitbucket.org/KJ_Duncan/jse-security-guide/src/master/
               learning from others in the hope my mistakes are new and interesting
               the temple of doom:
                    https://cwe.mitre.org/data/definitions/285.html
                    https://cwe.mitre.org/data/definitions/287.html
                    https://cwe.mitre.org/data/definitions/295.html
                    https://cwe.mitre.org/data/definitions/759.html
                    https://cwe.mitre.org/data/definitions/916.html */
      showLoginScreen()
      errorNotification("Login attempt failed", "Check your credentials and try again.", Pos.CENTER)
    }
  }

  private fun logout(quit: Boolean) {
    /* TEST:[LoginController::logout] check call/implementation imapDisconnect option here */
    if (find<IMAPController>().imapConnected()) find<IMAPController>().imapDisconnect()

    if (quit) Platform.exit()

    showLoginScreen()
  }

  init {
    subscribe<Logout> {
      logout(it.quit)
    }
  }
}
