package controller

import StoreBuilder
import model.SendMessageModel
import org.simplejavamail.api.email.AttachmentResource
import org.simplejavamail.api.email.Email
import org.simplejavamail.api.email.EmailPopulatingBuilder
import org.simplejavamail.api.mailer.Mailer
import org.simplejavamail.email.EmailBuilder
import org.simplejavamail.mailer.MailerBuilder
import tornadofx.*
import java.io.File
import javax.activation.FileDataSource

class SMTPController : Controller() {

  /* FIXME:[SMTPController::StoreBuilder] implement via an event bus */
  private lateinit var mStore: StoreBuilder

  fun setSession(store: StoreBuilder) { mStore = store; }

  /* FIXME:[SMTPController::smtpConnected] does not connect, version bump altered api. */
  fun smtpConnected(): Boolean =
    MailerBuilder
      .usingSession(mStore.session)
      .buildMailer()
      .testConnection()
      .equals(Unit)

  /* NOTE:[SMTPController::smtp-connection-pool]
      The library aims to improve performance for sending emails using Jakarta Mail.
      https://github.com/simple-java-mail/smtp-connection-pool */
  /* FIXME:[SMTPController::smtpOutlookMailBuilder] getOrThrow need to catch and log exception. */
  fun smtpOutlookMailBuilder(): Mailer =
    kotlin.runCatching {
      MailerBuilder
        .usingSession(mStore.session)
        .withSessionTimeout(10 * 1000)  // 10 seconds for bulk sending may be to quick
        .buildMailer()
    }.getOrThrow()

  fun sendMessageBuilder(model: SendMessageModel): Email {
    val message: EmailPopulatingBuilder = sendMailBuilder(model)
    if (checkForAttachments(model)) {
      message.withAttachments(addAttachments(model.attachment.value))
    }
    return message.buildEmail()
  }

  private fun checkForAttachments(model: SendMessageModel): Boolean =
    model.attachment.value.isNullOrEmpty().not()

  /* NOTE:[sendMailBuilder] GENERIC http://www.simplejavamail.org/features.html#section-add-recipients */
  private fun sendMailBuilder(model: SendMessageModel): EmailPopulatingBuilder =
    EmailBuilder.startingBlank()
      .from(model.senderName.valueSafe, model.senderAddress.valueSafe)
      /* TEST::[sendMailBuilder] SimpleListProperty<Address> is now a list, check for iteration: model.recipientAddress.value */
      .to(model.recipientName.valueSafe, model.recipientAddress.joinToString())
      .withSubject(model.subject.valueSafe)
      .withHTMLText(model.content.valueSafe)

  private fun addAttachments(files: List<File>): List<AttachmentResource> =
    files.fold(emptyList<AttachmentResource>()) { acc: List<AttachmentResource>, file: File ->
      acc + AttachmentResource(file.name, FileDataSource(file))
    }
}
