package model

data class Inbox(val folderName: String, val unread: Int, val messages: List<ReceivedMessage>)

