package model

import java.util.Date
import javax.mail.Address

data class Sentbox(val date: Date, val receiver: List<Address>, val subject: String, val content: String)

