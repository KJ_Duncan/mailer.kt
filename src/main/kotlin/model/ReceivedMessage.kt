package model

import java.util.Date
import javax.mail.Address

data class ReceivedMessage(val date: Date, val from: List<Address>, val subject: String, val content: String)
