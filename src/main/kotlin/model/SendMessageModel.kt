package model

import javafx.beans.property.Property
import javafx.beans.property.SimpleListProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import tornadofx.*
import java.io.File
import java.util.Date
import javax.mail.Address

/* NOTE: ItemViewModel<EmailSender> if you wish to keep a record of sent emails during runtime */
/* TEST:[SentMessageModel] var send, check val implementation instead */
class SendMessageModel(private var send: SendMessage = SendMessage()) : ViewModel() {
  val date: SimpleObjectProperty<Date> = bind { send.dateProperty }
  val senderName: SimpleStringProperty = bind { send.senderNameProperty }
  val senderAddress: SimpleStringProperty = bind { send.senderAddressProperty }
  val recipientName: SimpleStringProperty = bind { send.recipientNameProperty }
  val recipientAddress: SimpleListProperty<Address> = bind { send.recipientAddressProperty }
  val subject: SimpleStringProperty = bind { send.subjectProperty }
  val attachment: Property<List<File>> = bind { send.attachmentProperty }
  val content: SimpleStringProperty = bind { send.contentProperty }
}

/*
class SendMessageScope : Scope() {
  val sendMessageModel = SendMessageModel()
}
 */
