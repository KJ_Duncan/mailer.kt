package model

import javafx.beans.property.SimpleListProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import javafx.collections.ObservableList
import tornadofx.*
import java.io.File
import java.util.Date
import javax.mail.Address

class SendMessage {
  val dateProperty: SimpleObjectProperty<Date> =
    SimpleObjectProperty<Date>()
  var date: Date by dateProperty

  val senderNameProperty: SimpleStringProperty =
    SimpleStringProperty()
  var senderName: String by senderNameProperty

  val senderAddressProperty: SimpleStringProperty =
    SimpleStringProperty()
  var senderAddress: String by senderAddressProperty

  val recipientNameProperty: SimpleStringProperty =
    SimpleStringProperty()
  var recipientName: String by recipientNameProperty

  // val recipientAddressProperty: SimpleStringProperty =
  //   SimpleStringProperty()
  // var recipientAddress: String by recipientAddressProperty

  val recipientAddressProperty: SimpleListProperty<Address> =
    SimpleListProperty<Address>()
  var recipientAddress: ObservableList<Address> by recipientAddressProperty

  val subjectProperty: SimpleStringProperty =
    SimpleStringProperty()
  var subject: String by subjectProperty

  val attachmentProperty: SimpleObjectProperty<List<File>> =
    SimpleObjectProperty<List<File>>()
  var attachment: List<File> by attachmentProperty

  val contentProperty: SimpleStringProperty =
    SimpleStringProperty()
  var content: String by contentProperty
}
