package model

/*
 * Copyright (c) 2019 Razeware LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
 * distribute, sublicense, create a derivative work, and/or sell copies of the
 * Software in any work that is designed, intended, or marketed for pedagogical or
 * instructional purposes related to programming, coding, application development,
 * or information technology.  Permission for such use, copying, modification,
 * merger, publication, distribution, sublicensing, creation of derivative works,
 * or sale is expressly withheld.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
/**
 * 'The trie (pronounced try) is a tree that specializes in storing data that can
 *  be represented as a collection, such as English words' (Galata 2019, p.191).
 *
 * 'Tries provide great performance metrics in regards to prefix matching' (Galata 2019, p.203).
 *
 * @author Galata, I 2019, Data Structures & Algorithms in Kotlin, Tries, chpt 10, pp.191-203,
 *         raywenderlich.com, https://store.raywenderlich.com/products/data-structures-and-algorithms-in-kotlin
 *
 * @author Sedgewick, R & Flajolet, P 2015, String and Tries, An Introduction to the Analysis of Algorithms, 
 *         2nd ed, chpt 8, viewed 21 Apr 2020, https://aofa.cs.princeton.edu/home/ 
 *
 * @author Sedgewick, R & Wayne, K 2020, Tries, Algorithms, 4th ed, chpt 5.2, viewed 22 Apr 2020,
 *         https://algs4.cs.princeton.edu/home/
 *
 * @author Baldwin, D & Scragg, G 2004, Algorithms and data structures: the science of computing,
 *         1st ed, Charles River Media, Hingham, Mass.
 *
 * @author Saumont, P-Y 2019, The Joy of Kotlin, Working with laziness, chpt 9, 
 *         Manning Publications, https://www.manning.com/books/the-joy-of-kotlin 
 */
internal class Trie<Key> {
  /* TODO:[Trie::clear] check/cleanup function of contacts held in memory on logout */
  private val root: TrieNode<Key> =
    TrieNode<Key>(key = null, parent = null)

  private val storedLists: MutableSet<List<Key>> = mutableSetOf<List<Key>>()

  val lists: List<List<Key>>
    get() = storedLists.toList()

  val count: Int
    get() = storedLists.count()

  val isEmpty: Boolean
    get() = storedLists.isEmpty()

  /* TEST:[Trie::toList] traversing different data structures; Array<T>, ArrayList<T>, List<T>, LinkedList<T>...
       A for loop is a lazy control structure, 
       A list is a strict data structure (Kucherenko 2018, p.106). 
       call by name (lazy), 
       call by value (strict). */
  fun insert(list: List<Key>) {
    var current: TrieNode<Key> = root

    list.forEach { element: Key ->
      if (current.children[element] == null) {
        current.children[element] = TrieNode(element, current)
      }
      current = (current.children[element] ?: return@forEach)
    }
    current.isTerminating = true

    storedLists.add(list)
  }

  fun contains(list: List<Key>): Boolean {
    var current: TrieNode<Key> = root

    list.forEach { element: Key ->
      val child: TrieNode<Key> = current.children[element] ?: return false
      current = child
    }

    return current.isTerminating
  }

  fun remove(list: List<Key>) {
    var current: TrieNode<Key> = root

    list.forEach { element: Key ->
      val child: TrieNode<Key> = current.children[element] ?: return
      current = child
    }

    if (!current.isTerminating) return

    storedLists.remove(list)

    current.isTerminating = false

    while (current.parent != null && current.children.isEmpty() && !current.isTerminating) {
      (current.parent ?: return).children.remove(current.key)
      current = (current.parent ?: return)
    }
  }

  fun collections(prefix: List<Key>): List<List<Key>> {
    var current: TrieNode<Key> = root

    prefix.forEach { element: Key ->
      val child: TrieNode<Key> = current.children[element] ?: return emptyList()
      current = child
    }

    return collections(prefix, current)
  }

  private fun collections(prefix: List<Key>, node: TrieNode<Key>?): List<List<Key>> {
    val results: MutableList<List<Key>> = mutableListOf<List<Key>>()

    if (node?.isTerminating == true) {
      results.add(prefix)
    }

    node?.children?.forEach { (key: Key, node: TrieNode<Key>) ->
      results.addAll(collections(prefix + key, node))
    }

    return results
  }
}

/* TODO:[Trie::TrieNode<Key>] implement a non-nullable base case (identity)
*   initial thoughts Key is in [A-Z][a-z][0-9] uses UTF-8 which is a vector whats the origin?
*   null char (`\u0000'), not a char ('\uFFFF')
*        Char.MIN_VALUE,        Char.MAX_VALUE
*   can we restrict the domain further -> InternetAddress(address).validate()
*   front-end regex on user input to restrict the domain; simple, easy, and effective (no down stream error propagation).
*   Key == Char */
private class TrieNode<Key>(var key: Key?, var parent: TrieNode<Key>?) {

  val children: HashMap<Key, TrieNode<Key>> = HashMap()

  var isTerminating: Boolean = false
}

/* TEST:[Trie::toList] String.toCharArray, String.to*...String.as*, traversing different data structures
     see https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-char-array/
         https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.collections/to-typed-array.html */ 
internal fun Trie<Char>.insert(string: String) {
  insert(string.toList())
}

internal fun Trie<Char>.contains(string: String): Boolean {
  return contains(string.toList())
}

internal fun Trie<Char>.remove(string: String) {
  remove(string.toList())
}

internal fun Trie<Char>.collections(prefix: String): List<String> {
  return collections(prefix.toList()).map { it.joinToString(separator = "") }
}

/* TODO:[Trie::example] print all contacts loaded to a TreeView UI */
infix fun String.example(function: () -> Unit) {
  println("---Example of $this---")
  function()
  println()
}

