package app

import javafx.scene.input.KeyCode
import javafx.scene.input.KeyCodeCombination
import javafx.stage.Stage
import tornadofx.*
import view.LoginView

/* NOTE:[MailerApp::App] internationalisation of java applications https://docs.oracle.com/javase/tutorial/i18n/index.html */
class MailerApp : App(LoginView::class) { //Styles::class
  override fun start(stage: Stage) {
    super.start(stage)
  }

  /* NOTE:[MailerApp::CommandLine] startup through the main function */
  // fun main(args: Array<String>) {
  //   tornadofx.launch<MailerApp>(args)
  // }

  /* REMOVE:[MailerApp] init block */
  init {
    FX.layoutDebuggerShortcut = KeyCodeCombination(KeyCode.J, KeyCodeCombination.CONTROL_DOWN)
    // reloadStylesheetsOnFocus()
    reloadViewsOnFocus()
  }
}
