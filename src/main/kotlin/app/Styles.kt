package app

import javafx.geometry.Pos
import javafx.scene.paint.Color
import javafx.scene.text.FontWeight
import tornadofx.*

/* TODO:[Styles] implement css stylesheets */
// NOTE: https://docs.oracle.com/javase/8/javafx/layout-tutorial/size_align.htm#JFXLY133
// https://docs.oracle.com/javase/8/javafx/user-interface-tutorial/apply-css.htm#CHDGHCDG
// https://github.com/controlsfx/controlsfx/blob/master/controlsfx-samples/src/main/java/org/controlsfx/samples/HelloGlyphFont.java
class Styles : Stylesheet() {
  // val fontAwesome: GlyphFont = GlyphFontRegistry.font("FontAwesome")

  /*
    px: pixels, relative to the viewing device
    em: the 'font-size' of the relevant font
    ex: the 'x-height' of the relevant font
   */
  companion object {
    val inboxButton: CssRule by cssclass()
    val disconnectButton: CssRule by cssclass()
    val quitButton: CssRule by cssclass()
    val topViewButtons: CssRule by cssclass()
    val masterViewTop: CssRule by cssclass()
    val topViewLabel: CssRule by cssclass()
    val topViewComboBox: CssRule by cssclass()
    val listCell: CssRule by cssclass()
    val topViewButtonBar: CssRule by cssclass()
  }

  init {
    val topViewMixin: CssSelectionBlock = mixin {
      fontSize = 12.em
      fontWeight = FontWeight.LIGHT
      textFill = Color.WHITESMOKE
      backgroundColor += Color.DARKGRAY
    }

    masterViewTop {
      // maxWidth = 820.px
      // maxHeight = 320.px
      // minWidth = 620.px
      // minHeight = 220.px
      // backgroundColor += Color.DARKGRAY
    }

    listCell {
      and(selected) {
        +topViewMixin
      }
    }

    topViewLabel {
      +topViewMixin
      alignment = Pos.CENTER
    }

    topViewButtonBar {
      // maxWidth = 320.px
      // minWidth = maxWidth
      // alignment = Pos.CENTER
    }

    topViewButtons {
      +topViewMixin
      // maxWidth = Double.MAX_VALUE.em
      spacing = 10.px
    }

    topViewComboBox {
      +topViewMixin
    }

    inboxButton {
      // FontAwesomeIconView(FontAwesomeIcon.INBOX)
      // FontAwesome.Glyph.INBOX
    }

    disconnectButton {
      borderColor += box(Color.FORESTGREEN)
    }

    quitButton {
      borderColor += box(Color.FIREBRICK)
    }
  }
}
