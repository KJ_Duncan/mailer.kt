package prop

/*
 * @author Jakarta EE 2020, SSLNOTES.txt, Jakarta Mail, Eclipse Foundation,
 *         viewed 28 May 2020, https://eclipse-ee4j.github.io/mail/docs/SSLNOTES.txt
 */
internal enum class JakartaMailProperty(private val key: String) {
  MAIL_DEBUG_boolean("mail.debug"),
  MAIL_STORE_PROTOCOL_string("mail.store.protocol"),
  MAIL_IMAPS_HOST_string("mail.imaps.host"),
  MAIL_IMAPS_PORT_int("mail.imaps.port"),
  MAIL_IMAPS_USER_string("mail.imaps.user"),
  MAIL_IMAPS_PEEK_boolean("mail.imaps.peek"),
  MAIL_IMAP_STARTTLS_boolean("mail.imap.starttls.enable"),
  MAIL_SMTP_STARTTLS_boolean("mail.smtp.starttls.enable");

  fun key(): String = this.key
}

internal enum class SimpleJavaMailProperty(private val key: String) {
  SIMPLEJAVAMAIL_JAVAXMAIL_DEBUG_boolean("simplejavamail.javaxmail.debug"),
  SIMPLEJAVAMAIL_TRANSPORT_MODE_LOGGING_ONLY_boolean("simplejavamail.transport.mode.logging.only"),
  SIMPLEJAVAMAIL_TRANSPORTSTRATEGY_api("simplejavamail.transportstrategy"),
  SIMPLEJAVAMAIL_SMTP_HOST_string("simplejavamail.smtp.host"),
  SIMPLEJAVAMAIL_SMTP_PORT_int("simplejavamail.smtp.port"),
  SIMPLEJAVAMAIL_SMTP_USERNAME_string("simplejavamail.smtp.username");

  fun key(): String = this.key
}

internal enum class MailerContactProperty(private val key: String) {
  CONTACT_ADDRESS_string("contact.address"),
  CONTACT_LIST_string("contact.list"),
  CONTACT_GROUP_string("contact.group");

  fun key(): String = this.key
}

/* Used in /view/SettingsView */
enum class UserSettingProperty(private val key: String) {
  RENDER_HTML_api("render-html"),
  OPEN_ATTACHMENT_boolean("open-attachment"),
  ALIAS_EMAIL_string("alias-email");

  fun key(): String = this.key
}

/* type checks available options IMAPController.checkPropsAndRenderHtml / SettingsView */
sealed class RenderHTML {
  object Plain : RenderHTML()
  object Format : RenderHTML()
  object Images : RenderHTML()
}
