package prop
import org.simplejavamail.api.mailer.config.TransportStrategy

/**
 * Outlook.com
 * IMAP setting
 *  Server name: outlook.office365.com
 *  Port: 993
 *  Encryption method: TLS
 *
 *  POP settings
 *   Server name: outlook.office365.com
 *   Port: 995
 *   Encryption method: TLS
 *
 * SMTP setting
 *  Server name: smtp.office365.com
 *  Port: 587
 *  Encryption method: STARTTLS
 *
 *  Gmail
 * IMAP setting
 *  Server name: imap.gmail.com
 *  Port: 993
 *  Encryption method: TLS
 *
 *  POP settings
 *   Server name: pop.gmail.com
 *   Port: 995
 *   Encryption method: TLS
 *
 * SMTP setting
 *  Server name: smtp.gmail.com
 *  Port: 465
 *  Encryption method: TLS  <-- double check
 *
 */
// type checks on radio-buttons in LoginView
sealed class HostAccount {
  object Office365 : HostAccount() {
    const val OUTLOOK: String = "Outlook"
    const val IMAPS_HOST: String = "outlook.office365.com"
    const val IMAPS_PROTCOL: String = "imaps"
    const val IMAPS_PORT: Int = 993

    const val SMTP_HOST: String = "smtp.office365.com"
    const val SMTP_PORT: Int = 587
    final val SMTP_TLS: TransportStrategy = TransportStrategy.SMTP_TLS
  }

  /* NOTE:[Gmail] http://www.simplejavamail.org/features.html#section-gmail */
  object Gmail : HostAccount() {
    const val GMAIL: String = "Gmail"
    const val IMAPS_HOST: String = "imap.gmail.com"
    const val IMAPS_PROTCOL: String = "imaps"
    const val IMAPS_PORT: Int = 993

    const val SMTP_HOST: String = "smtp.gmail.com"
    const val SMTP_PORT: Int = 465
    final val SMTP_TLS: TransportStrategy = TransportStrategy.SMTPS  // <-- double check documentation
  }
}
