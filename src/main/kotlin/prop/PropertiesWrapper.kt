package prop

import prop.HostAccount.Office365
import prop.JakartaMailProperty.*
import prop.RenderHTML.Format
import prop.SimpleJavaMailProperty.*
import prop.UserSettingProperty.*
import java.util.Properties

/* NOTE:[PropertiesWrapper] java.prop.Properties
    https://docs.oracle.com/en/java/javase/11/core/preferences-api1.html#GUID-66286D3D-67C9-4CF5-8C1E-0BE834F1C6EC
    Session.getInstance(props, auth) uses the Properties container, so unable to use java.prop.prefs */
object PropertiesWrapper {
  @JvmField
  val props: Properties = Properties()

  @JvmStatic
  fun outlookProperties(username: String) {
    /* REMOVE:[outlookPropertyBuilder] debug modes to false */
    props[MAIL_DEBUG_boolean.key()] = true
    props[MAIL_STORE_PROTOCOL_string.key()] = Office365.IMAPS_PROTCOL
    props[MAIL_IMAPS_HOST_string.key()] = Office365.IMAPS_HOST
    props[MAIL_IMAPS_PORT_int.key()] = Office365.IMAPS_PORT
    props[MAIL_IMAPS_USER_string.key()] = username
    props[MAIL_IMAPS_PEEK_boolean.key()] = true  // avoids setting seen flags on messages
    props[MAIL_IMAP_STARTTLS_boolean.key()] = true  // FIXME: check outlook supports starttls over imap
    props[MAIL_SMTP_STARTTLS_boolean.key()] = true  // FIXME: enables over smtp, check for/if overlap with simplejavamail

    props[SIMPLEJAVAMAIL_JAVAXMAIL_DEBUG_boolean.key()] = true
    props[SIMPLEJAVAMAIL_TRANSPORT_MODE_LOGGING_ONLY_boolean.key()] = true
    props[SIMPLEJAVAMAIL_TRANSPORTSTRATEGY_api.key()] = Office365.SMTP_TLS
    props[SIMPLEJAVAMAIL_SMTP_HOST_string.key()] = Office365.SMTP_HOST
    props[SIMPLEJAVAMAIL_SMTP_PORT_int.key()] = Office365.SMTP_PORT
    props[SIMPLEJAVAMAIL_SMTP_USERNAME_string.key()] = username

    userSettings()
  }

  @JvmStatic
  fun gmailProperties(username: String): Nothing {
    TODO("Gmail property builder")

    userSettings()
  }

  /* FIXME:[PropertiesWrapper::userSettings] put back to Plain as the default setting */
  private fun userSettings() {
    props[RENDER_HTML_api.key()] = Format
    props[OPEN_ATTACHMENT_boolean.key()] = false
    props[ALIAS_EMAIL_string.key()] = ""
  }
}
