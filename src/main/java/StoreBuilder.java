import io.Console;
import javafx.geometry.Pos;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;
import org.jetbrains.annotations.NotNull;

import javax.mail.Folder;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Store;
import javax.mail.event.StoreEvent;

/*
 * Make class unclonable:
 *
 *   public final Object clone() throws java.lang.CloneNotSupportedException {
 *     throw new java.lang.CloneNotSupportedException();
 *   }
 *
 * Make class unserialiseable:
 *
 *   private final void writeObject(ObjectOutputStream out)
 *     throws java.io.IOException {
 *       throw new java.io.IOException("Object cannot be serialized");
 *   }
 *
 * Make class undeserialiseable:
 *
 *   private final void readObject(ObjectInputStream in)
 *     throws java.io.IOException {
 *       throw new java.io.IOException("Class cannot be deserialized");
 *   }
 *
 * @author Wheeler, D 2019, Language-Specific Issues, 10.6. Java,
 *         Secure Programming HOWTO, viewed 26 May 2020,
 *         https://dwheeler.com/secure-programs/Secure-Programs-HOWTO.html#JAVA
 */
public class StoreBuilder extends AbstractSessionBuilder {
  @NotNull
  private Store store;

  private StoreBuilder(@NotNull final String username,
                       @NotNull final char[] password,
                       @NotNull final String host) {
   super(username, password, host);
   this.connect();
   /* Do not mark as final and null out here or in / see super
    *    Arrays.fill(password, ' ');
    *    password = null;
    */
  }

  public static StoreBuilder createStoreBuilder(@NotNull final String username,
                                                @NotNull final char[] password,
                                                @NotNull final String host) {
    return new StoreBuilder(username, password, host);
  }

  @Override
  protected void connect() {
    try {
      setStore();
      addStoreListener();
      this.store.connect(); // may need to pass username, password
    } catch (final NoSuchProviderException nspe) {
      Console.showDebug.invoke(nspe.getMessage());
    } catch (final MessagingException me) {
      Console.showDebug.invoke(me.getMessage());
    }
  }

  @Override
  public void close() {
    try {
      this.store.close();
    } catch (final NoSuchProviderException nspe) {
      Console.showDebug.invoke(nspe.getMessage());
    } catch (final MessagingException me) {
      Console.showDebug.invoke(me.getMessage());
    }
  }

  @Override
  public boolean isConnected() {
    return this.store.isConnected();
  }

  private void setStore() {
    try {
      this.store = getSession().getStore();
    } catch (NoSuchProviderException e) {
      Console.showDebug.invoke(e.getMessage());
    }
  }

  @NotNull
  public final Folder getStoreFolder(final String folder) throws MessagingException {
    return this.store.getFolder(folder);
  }

  public final Folder getDefaultFolder() throws MessagingException {
    return this.store.getDefaultFolder();
  }

  private void addStoreListener() {
    this.store.addStoreListener(e -> {
      if (StoreEvent.ALERT == e.getMessageType()) {
        Notifications
            .create()
            .title("ALERT")
            .text(e.getMessage())
            .position(Pos.TOP_RIGHT)
            .hideAfter(Duration.seconds(5.0))
            .showWarning();
      } else {
        Notifications
            .create()
            .title("NOTICE")
            .text(e.getMessage())
            .position(Pos.TOP_RIGHT)
            .hideAfter(Duration.seconds(5.0))
            .showInformation();
      }
    });
  }
}
