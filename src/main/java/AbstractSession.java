import org.jetbrains.annotations.NotNull;

import javax.mail.Session;

abstract class AbstractSession {
  @NotNull
  private Session mSession;
  @NotNull
  private final String mHost;
  @NotNull
  private final String mUserName;

  AbstractSession(@NotNull final String userName, @NotNull final String host) {
    mUserName = userName;
    mHost = host;
  }

  @NotNull
  public final Session getSession() {
    return this.mSession;
  }

  protected final void setSession(@NotNull final Session session) {
    this.mSession = session;
  }

  @NotNull
  public final String getHost() {
    return this.mHost;
  }

  @NotNull
  public final String getUserName() {
    return this.mUserName;
  }

  @NotNull
  @Override
  public final String toString() {
    return "Session(" +
        "username=" + mUserName +
        ", host=" + mHost +
        ')';
  }

  protected abstract void connect();

  abstract void close();

  abstract boolean isConnected();
}
