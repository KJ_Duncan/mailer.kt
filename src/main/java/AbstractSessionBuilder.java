import org.jetbrains.annotations.NotNull;
import prop.HostAccount;
import prop.PropertiesWrapper;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import java.util.Arrays;
import java.util.Properties;

abstract class AbstractSessionBuilder extends AbstractSession {

  AbstractSessionBuilder(@NotNull final String username,
                         @NotNull char[] password,
                         @NotNull final String host) {
    super(username, host);
    this.setProps();
    this.setSession(init(username, password));
    Arrays.fill(password, ' ');
    password = null;
  }

  @NotNull
  private Session init(String username, char[] password) {
    /* java.net.Authenticator, java.net.PasswordAuthentication
       is not compatable with javax.mail.Session */
    return Session.getDefaultInstance(getProps(), new Authenticator() {
      @NotNull
      @Override
      protected PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(username, password.clone().toString());
      }
    });
  }

  private void setProps() {
    if (!this.getHost().isEmpty() && HostAccount.Office365.OUTLOOK.equals(this.getHost())) {
      PropertiesWrapper.outlookProperties(this.getUserName());
    }
    if (!this.getHost().isEmpty() && HostAccount.Gmail.GMAIL.equals(this.getHost())) {
      PropertiesWrapper.gmailProperties(this.getUserName());
    }
  }

  @NotNull
  private static Properties getProps() {
    return PropertiesWrapper.props;
  }
}
