// reproducible tests on single instance
class StoreController {
  private StoreBuilder storeBuilder;
  StoreController(StoreBuilder storeBuilder) {
    this.storeBuilder = storeBuilder;
  }
  StoreBuilder getStoreBuilder() {
    return storeBuilder;
  }
}
