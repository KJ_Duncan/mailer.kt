package prop

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Test
import org.simplejavamail.api.mailer.config.TransportStrategy

internal class HostAccountTest {

  @Test
  fun `field OUTLOOK does not equal Gmail`() {
    assertFalse(HostAccount.Office365.OUTLOOK == "Gmail")
  }

  @Test
  fun `field OUTLOOK equals Outlook`() {
    assertEquals("Outlook", HostAccount.Office365.OUTLOOK)
  }

  @Test
  fun `field IMAPS_HOST equals outlook Office365 com`() {
    assertEquals("outlook.Host.Office365.com", HostAccount.Office365.IMAPS_HOST)
  }

  @Test
  fun `field IMAPS_PROTCOL equals imaps`() {
    assertEquals("imaps", HostAccount.Office365.IMAPS_PROTCOL)
  }

  @Test
  fun `field IMAPS_PORT equals 993`() {
    assertEquals(993, HostAccount.Office365.IMAPS_PORT)
  }

  @Test
  fun `field SMTP_HOST equals smtp Office365 com`() {
    assertEquals("smtp.Host.Office365.com", HostAccount.Office365.SMTP_HOST)
  }

  @Test
  fun `field SMTP_PORT equals 587`() {
    assertEquals(587, HostAccount.Office365.SMTP_PORT)
  }

  @Test
  fun `field SMTP_TLS equals TransportStrategy SMTP_TLS`() {
    assertEquals(TransportStrategy.SMTP_TLS, HostAccount.Office365.SMTP_TLS)
  }
}
