package prop

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

class MailPropertyTest {

  @Nested
  inner class JakartaMailPropertyTest {
    @Test
    fun `jakarta mail properties are not null`() {
      assertNotNull(JakartaMailProperty.MAIL_IMAPS_USER_string)
      assertNotNull(JakartaMailProperty.MAIL_DEBUG_boolean)
      assertNotNull(JakartaMailProperty.MAIL_IMAPS_HOST_string)
      assertNotNull(JakartaMailProperty.MAIL_IMAPS_PEEK_boolean)
      assertNotNull(JakartaMailProperty.MAIL_IMAPS_PORT_int)
      assertNotNull(JakartaMailProperty.MAIL_STORE_PROTOCOL_string)
    }

    @Test
    fun `debug key equals mail debug`() {
      assertEquals("mail.debug", JakartaMailProperty.MAIL_DEBUG_boolean.key())
    }

    @Test
    fun `protocol key equals mail store protocol`() {
      assertEquals("mail.store.protocol", JakartaMailProperty.MAIL_STORE_PROTOCOL_string.key())
    }

    @Test
    fun `host key equals mail imaps host`() {
      assertEquals("mail.imaps.host", JakartaMailProperty.MAIL_IMAPS_HOST_string.key())
    }

    @Test
    fun `port key equals mail imaps port`() {
      assertEquals("mail.imaps.port", JakartaMailProperty.MAIL_IMAPS_PORT_int.key())
    }

    @Test
    fun `user key equals mail imaps user`() {
      assertEquals("mail.imaps.user", JakartaMailProperty.MAIL_IMAPS_USER_string.key())
    }

    @Test
    fun `peek key equals mail imaps peek`() {
      assertEquals("mail.imaps.peek", JakartaMailProperty.MAIL_IMAPS_PEEK_boolean.key())
    }
  }

  @Nested
  inner class SimpleJavaMailPropertyTest {
    @Test
    fun `simple java mail properties are not null`() {
      assertNotNull(SimpleJavaMailProperty.SIMPLEJAVAMAIL_JAVAXMAIL_DEBUG_boolean)
      assertNotNull(SimpleJavaMailProperty.SIMPLEJAVAMAIL_SMTP_HOST_string)
      assertNotNull(SimpleJavaMailProperty.SIMPLEJAVAMAIL_SMTP_PORT_int)
      assertNotNull(SimpleJavaMailProperty.SIMPLEJAVAMAIL_SMTP_USERNAME_string)
      assertNotNull(SimpleJavaMailProperty.SIMPLEJAVAMAIL_TRANSPORTSTRATEGY_api)
      assertNotNull(SimpleJavaMailProperty.SIMPLEJAVAMAIL_TRANSPORT_MODE_LOGGING_ONLY_boolean)
    }

    @Test
    fun `debug key equals simplejavamail javaxmail debug`() {
      assertEquals("simplejavamail.javaxmail.debug", SimpleJavaMailProperty.SIMPLEJAVAMAIL_JAVAXMAIL_DEBUG_boolean.key())
    }

    @Test
    fun `logging key equals simplejavamail transport mode logging only`() {
      assertEquals(
        "simplejavamail.transport.mode.logging.only", SimpleJavaMailProperty.SIMPLEJAVAMAIL_TRANSPORT_MODE_LOGGING_ONLY_boolean.key())
    }

    @Test
    fun `transport strategy key equals simplejavamail transportstrategy`() {
      assertEquals("simplejavamail.transportstrategy", SimpleJavaMailProperty.SIMPLEJAVAMAIL_TRANSPORTSTRATEGY_api.key())
    }

    @Test
    fun `host key equals simplejavamail smtp host`() {
      assertEquals("simplejavamail.smtp.host", SimpleJavaMailProperty.SIMPLEJAVAMAIL_SMTP_HOST_string.key())
    }

    @Test
    fun `port key equals simplejavamail smtp port`() {
      assertEquals("simplejavamail.smtp.port", SimpleJavaMailProperty.SIMPLEJAVAMAIL_SMTP_PORT_int.key())
    }

    @Test
    fun `username key equals simplejavamail smtp username`() {
      assertEquals("simplejavamail.smtp.username", SimpleJavaMailProperty.SIMPLEJAVAMAIL_SMTP_USERNAME_string.key())
    }
  }

  @Nested
  inner class MailerContactPropertyTest {
    @Test
    fun `mailer contact properties are not null`() {
      assertNotNull(MailerContactProperty.CONTACT_ADDRESS_string)
      assertNotNull(MailerContactProperty.CONTACT_LIST_string)
      assertNotNull(MailerContactProperty.CONTACT_GROUP_string)
    }

    @Test
    fun `contact address key equals contact address`() {
      assertEquals("contact.address", MailerContactProperty.CONTACT_ADDRESS_string.key())
    }

    @Test
    fun `contact list key equals contact list`() {
      assertEquals("contact.list", MailerContactProperty.CONTACT_LIST_string.key())
    }

    @Test
    fun `contact group key equals contact group`() {
      assertEquals("contact.group", MailerContactProperty.CONTACT_GROUP_string.key())
    }
  }

  @Nested
  inner class UserSettingPropertyTest {
    @Test
    fun `user setting properties are not null`() {
      assertNotNull(UserSettingProperty.RENDER_HTML_api)
      assertNotNull(UserSettingProperty.OPEN_ATTACHMENT_boolean)
      assertNotNull(UserSettingProperty.ALIAS_EMAIL_string)
    }

    @Test
    fun `render html key equals render-html`() {
      assertEquals("render-html", UserSettingProperty.RENDER_HTML_api.key())
    }

    @Test
    fun `open attachment key equals open-attachment`() {
      assertEquals("open-attachment", UserSettingProperty.OPEN_ATTACHMENT_boolean.key())
    }

    @Test
    fun `alias email key equals alias-email`() {
      assertEquals("alias-email", UserSettingProperty.ALIAS_EMAIL_string.key())
    }
  }

  @Nested
  inner class RenderHTMLTest {
    @Test
    fun `render html objects are not null`() {
      assertNotNull(RenderHTML.Plain)
      assertNotNull(RenderHTML.Format)
      assertNotNull(RenderHTML.Images)
    }
  }
}
