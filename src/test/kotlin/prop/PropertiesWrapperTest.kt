package prop

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Assumptions.assumingThat
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.simplejavamail.api.mailer.config.TransportStrategy

internal class PropertiesWrapperTest {

  private val username: String = "properties-wrapper-test"

  init {
    PropertiesWrapper.outlookProperties(username)
  }

  @Test
  fun `props field is not null`() {
    assertNotNull(PropertiesWrapper.props)
  }

  @Nested
  @DisplayName("Outlook Properties")
  inner class OutlookPropertes {

    @Nested
    @DisplayName("IMAP")
    inner class ImapProperties {

      @Test
      fun `props imaps peek equals true`() {
        assumingThat(true.equals(PropertiesWrapper.props["mail.debug"] as Boolean)) {
          assertEquals(true, PropertiesWrapper.props["mail.imaps.peek"] as Boolean)
        }
      }

      @Test
      fun `props imaps peek equals false`() {
        assumingThat(false.equals(PropertiesWrapper.props["mail.debug"] as Boolean)) {
          assertEquals(false, PropertiesWrapper.props["mail.imaps.peek"] as Boolean)
        }
      }

      @Test
      fun `props debug equals true`() {
        assumingThat(true.equals(PropertiesWrapper.props["mail.imaps.peek"] as Boolean)) {
          assertEquals(true, PropertiesWrapper.props["mail.debug"] as Boolean)
        }
      }

      @Test
      fun `props debug equals false`() {
        assumingThat(false.equals(PropertiesWrapper.props["mail.imaps.peek"] as Boolean)) {
          assertEquals(false, PropertiesWrapper.props["mail.debug"] as Boolean)
        }
      }

      @Test
      fun `props store protocol equals imaps`() {
        assertEquals("imaps", PropertiesWrapper.props["mail.store.protocol"] as String)
      }

      @Test
      fun `props imaps host equals outlook office365 com`() {
        assertEquals("outlook.office365.com", PropertiesWrapper.props["mail.imaps.host"] as String)
      }

      @Test
      fun `props imaps port equals 993`() {
        assertEquals(993, PropertiesWrapper.props["mail.imaps.port"] as Int)
      }

      @Test
      fun `props imaps user equals properties-wrapper-test`() {
        assertEquals(username, PropertiesWrapper.props["mail.imaps.user"] as String)
      }
    }

    @Nested
    @DisplayName("SMTP")
    inner class SmtpProperties {

      @Test
      fun `props transport mode logging equals true`() {
        assumingThat(true.equals(PropertiesWrapper.props["simplejavamail.javaxmail.debug"] as Boolean)) {
          assertEquals(true, PropertiesWrapper.props["simplejavamail.transport.mode.logging.only"] as Boolean)
        }
      }

      @Test
      fun `props transport mode logging equals false`() {
        assumingThat(false.equals(PropertiesWrapper.props["simplejavamail.javaxmail.debug"] as Boolean)) {
          assertEquals(false, PropertiesWrapper.props["simplejavamail.transport.mode.logging.only"] as Boolean)
        }
      }

      @Test
      fun `props debug equals true`() {
        assumingThat(true.equals(PropertiesWrapper.props["simplejavamail.transport.mode.logging.only"] as Boolean)) {
          assertEquals(true, PropertiesWrapper.props["simplejavamail.javaxmail.debug"] as Boolean)
        }
      }

      @Test
      fun `props debug equals false`() {
        assumingThat(false.equals(PropertiesWrapper.props["simplejavamail.transport.mode.logging.only"] as Boolean)) {
          assertEquals(false, PropertiesWrapper.props["simplejavamail.javaxmail.debug"] as Boolean)
        }
      }

      @Test
      fun `props transport strategy equals smtp tls`() {
        assertEquals(TransportStrategy.SMTP_TLS, PropertiesWrapper.props["simplejavamail.transportstrategy"])
      }

      @Test
      fun `props smtp host equals smtp office365 com`() {
        assertEquals("smtp.office365.com", PropertiesWrapper.props["simplejavamail.smtp.host"] as String)
      }

      @Test
      fun `props smtp port equals 587`() {
        assertEquals(587, PropertiesWrapper.props["simplejavamail.smtp.port"] as Int)
      }

      @Test
      fun `props smtp username equals properties-wrapper-test`() {
        assertEquals(username, PropertiesWrapper.props["simplejavamail.smtp.username"] as String)
      }
    }
  }

  @Nested
  @DisplayName("Gmail Properties")
  inner class GmailProperties {

    @Nested
    @DisplayName("IMAP")
    inner class ImapProperties {

    }

    @Nested
    @DisplayName("SMTP")
    inner class SmtpProperties {

    }
  }
}
