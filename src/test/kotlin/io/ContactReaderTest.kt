package io

import model.Trie
import model.collections
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertDoesNotThrow

internal class ContactReaderTest {

  @Test
  @Disabled
  fun getContacts() {
    assertDoesNotThrow { ContactReader() }
    // Console.showInfo(ContactReader)
  }

  @Test
  @Disabled
  fun getTrie() {
    // Console.showInfo(ContactReader.trieLists().toString())
    Console.showInfo(ContactReader.trieContains("s").toString())
    Console.showInfo(ContactReader.trieCollections("s").toString())
  }

  @Test
  fun getAddresses() {
    val trie = Trie<Char>().apply {
      ContactReader()
      // ContactReader.addresses.foreach { insert(it) }
    }
    Console.showInfo(trie.lists.toString())
    Console.showInfo(trie.count.toString())
    Console.showInfo(trie.collections("s").toString())
    // Console.showInfo(ContactLoader.addresses.toString())
    // Console.showInfo(ContactLoader.prettyPrint)
    // Console.showInfo(ContactLoader.trieLists().toString())
    // Console.showInfo(ContactLoader.trieCollections("o").joinToString(transform = { it.removeSurrounding("[", "]") }))
  }
}
