package io

import org.junit.jupiter.api.MethodOrderer
import org.junit.jupiter.api.Order
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInfo
import org.junit.jupiter.api.TestMethodOrder
import org.junit.jupiter.api.assertDoesNotThrow
import org.junit.jupiter.api.assertThrows
import kotlin.time.ExperimentalTime

@ExperimentalTime
@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
internal class DurationTimerTest {

  @Test
  @Order(1)
  fun `exception absence testing`() {
    assertDoesNotThrow("Should not throw and exception") {
      DurationTimer.start()
    }

    assertDoesNotThrow("Should not throw and exception") {
      DurationTimer.stopMilliseconds()
    }
  }

  @Test
  @Order(2)
  fun `expected exception testing`() {
    DurationTimer.start()
    assertThrows<IllegalStateException>("Should throw an exception") {
      DurationTimer.start()
    }

    DurationTimer.stopMilliseconds()
    assertThrows<IllegalStateException>("Should throw an exception") {
      DurationTimer.stopMilliseconds()
    }
  }

  @Test
  @Order(3)
  fun testStopMillisecondsClassName() {
    assertDoesNotThrow {
      DurationTimer.start()
      DurationTimer.stopMilliseconds(this.javaClass)
    }
  }

  @Test
  @Order(4)
  fun testStopMillisecondsClassAndMethodName(testInfo: TestInfo) {
    assertDoesNotThrow {
      DurationTimer.start()
      DurationTimer.stopMilliseconds(this.javaClass, testInfo.testMethod.get())
    }
  }

  @Test
  @Order(5)
  fun stopSeconds() {
    assertDoesNotThrow {
      DurationTimer.start()
      DurationTimer.stopSeconds()
    }
  }

  @Test
  @Order(6)
  fun testStopSecondsClassName() {
    assertDoesNotThrow {
      DurationTimer.start()
      DurationTimer.stopSeconds(this.javaClass)
    }
  }

  @Test
  @Order(7)
  fun testStopSecondsClassAndMethodName(testInfo: TestInfo) {
    assertDoesNotThrow {
      DurationTimer.start()
      DurationTimer.stopSeconds(this.javaClass, testInfo.testMethod.get())
    }
  }

  @Test
  @Order(8)
  fun stopMinutes() {
    assertDoesNotThrow {
      DurationTimer.start()
      DurationTimer.stopMinutes()
    }
  }

  @Test
  @Order(9)
  fun testStopMinutesClassName() {
    assertDoesNotThrow {
      DurationTimer.start()
      DurationTimer.stopMinutes(this.javaClass)
    }
  }

  @Test
  @Order(10)
  fun testStopMinutesClassAndMethodName(testInfo: TestInfo) {
    assertDoesNotThrow {
      DurationTimer.start()
      DurationTimer.stopMinutes(this.javaClass, testInfo.testMethod.get())
    }
  }

  @Test
  @Order(11)
  fun stopAllDurations() {
    assertDoesNotThrow {
      DurationTimer.start()
      DurationTimer.stopAllDurations()
    }
  }

  @Test
  @Order(12)
  fun testStopAllDurationsClassName() {
    assertDoesNotThrow {
      DurationTimer.start()
      DurationTimer.stopAllDurations(this.javaClass)
    }
  }

  @Test
  @Order(13)
  fun testStopAllDurationsClassAndMethodName(testInfo: TestInfo) {
    assertDoesNotThrow {
      DurationTimer.start()
      DurationTimer.stopAllDurations(this.javaClass, testInfo.testMethod.get())
    }
  }
}
