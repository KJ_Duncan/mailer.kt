package model

import io.Console
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.MethodOrderer
import org.junit.jupiter.api.Order
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestMethodOrder
import org.junit.jupiter.api.assertDoesNotThrow

@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
internal class TrieTest {

  private val trie: Trie<Char> = Trie<Char>()

  private fun loadTrie() {
    trie.apply {
      insert("car")
      insert("card")
      insert("care")
      insert("cared")
      insert("cars")
      insert("carbs")
      insert("carapace")
      insert("cargo")
    }
  }

  @Test
  @Order(1)
  fun `assert trie is not null`() {
    assertNotNull(trie)
  }

  @Test
  @Order(2)
  fun `trie does not contain 'chars'`() {
    assertFalse(trie.contains("chars".toList()))
  }

  @Test
  @Order(3)
  fun `exception absence testing`() {
    assertDoesNotThrow { trie.contains("chars".toList()) }
    assertDoesNotThrow { trie.remove("chars".toList()) }
    assertDoesNotThrow { trie.insert("chars".toList()) }
    assertDoesNotThrow { trie.contains("chars".toList()) }
    assertDoesNotThrow { trie.remove("chars".toList()) }
    assertDoesNotThrow { trie.contains("chars".toList()) }
    assertDoesNotThrow { trie.collections("c") }
  }

  @Test
  fun `expected exception testing`() {
    "prefix matching" example {
      loadTrie()
      Console.showInfo(trie.lists.toString())
      println("\nCollections starting with \"car\"")
      val prefixCar = trie.collections("car")
      println(prefixCar)

      println("\nCollections starting with \"care\"")
      val prefixCare = trie.collections("care")
      println(prefixCare)
    }
  }

  @Test
  @Order(4)
  fun `getCount`() {
    assertEquals(0, trie.count)
  }

  @Test
  @Order(5)
  fun `isEmpty`() {
    assertTrue(trie.isEmpty)
    loadTrie()
    assertFalse(trie.isEmpty)
  }

  @Test
  @Order(6)
  fun `getList`() {
    val list = listOf<String>("car", "carapace", "carbs", "cars", "card", "care", "cared", "cargo")
    assertEquals(list, trie.collections("car"))
    assertEquals(list.size, trie.count)
  }

  @Test
  @Disabled
  fun contains() {
  }

  @Test
  @Disabled
  fun remove() {
  }

  @Test
  @Disabled
  fun collections() {
  }
}
