package model

import io.mockk.mockk
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import java.util.Date
import javax.mail.Address

internal class SentboxTest {

  private val sentbox: Sentbox = setUpModel()

  private fun setUpModel(): Sentbox {
    val address: Address = mockk<Address>(relaxed = true)

    return Sentbox(
      date = Date(),
      receiver = listOf(address),
      subject = "message subject",
      content = "message content"
    )
  }

  @Test
  fun `SentBoxPOKO is not null`() {
    assertNotNull(sentbox)
  }

  @Test
  fun getDate() {
    assertEquals(Date().toString(), sentbox.date.toString())
  }

  @Test
  fun getReceiver() {
    assertNotNull(sentbox.receiver)
    assertEquals(1, sentbox.receiver.size)
  }

  @Test
  fun getSubject() {
    assertEquals("message subject", sentbox.subject)
  }

  @Test
  fun getContent() {
    assertEquals("message content", sentbox.content)
  }
}
