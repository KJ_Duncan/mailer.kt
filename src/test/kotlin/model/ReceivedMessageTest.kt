package model

import io.mockk.mockk
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.util.Date
import javax.mail.Address

internal class ReceivedMessageTest {

  private val address: Address = mockk<Address>(relaxed = true)
  private val date: Date = mockk<Date>(relaxed = true)
  private val receivedMessage: ReceivedMessage = ReceivedMessage(
    date = date,
    from = listOf(address),
    subject = "ReceivedPOKOTest",
    content = "mail content"
  )

  @Test
  fun `date equals Date`() {
    assertEquals(date, receivedMessage.date)
  }

  @Test
  fun `from is a list of size 1`() {
    assertEquals(1, receivedMessage.from.size)
  }

  @Test
  fun `from is contains an Address`() {
    assertEquals(address, receivedMessage.from[0])
  }

  @Test
  fun `subject equals ReceivedPOKOTest`() {
    assertEquals("ReceivedPOKOTest", receivedMessage.subject)
  }

  @Test
  fun `content equals mail content`() {
    assertEquals("mail content", receivedMessage.content)
  }
}
