package model

import io.mockk.mockk
import javafx.collections.ObservableList
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import java.io.File
import java.util.Date
import javax.mail.internet.InternetAddress

internal class SendMessageModelTest {

  private val sendMessageModel: SendMessageModel = setUpModel()

  private fun setUpModel(): SendMessageModel {
    val sendMessage = SendMessage()
    val dateMock: Date = mockk<Date>(relaxed = true)
    val observableListMock = mockk<ObservableList<File>>(relaxed = true)

    // sendMessage.id = "one"
    sendMessage.date = dateMock
    sendMessage.senderName = "sender name"
    sendMessage.senderAddress = "sender@address"
    sendMessage.recipientName = "recipient name"
    sendMessage.recipientAddress.add(InternetAddress("recipient@address"))
    sendMessage.subject = "message subject"
    sendMessage.attachment = observableListMock
    sendMessage.content = "message content"

    return SendMessageModel(sendMessage)
  }

  @Test
  fun `SendMessageModel is not null`() {
    assertNotNull(sendMessageModel)
  }

  @Test
  @Disabled("id implementation from email receipt confirmation number")
  fun getId() {
    // assertEquals("one", sendMessageModel.id.valueSafe)
  }

  @Test
  fun getDate() {
    assertNotNull(sendMessageModel.date.value)
  }

  @Test
  fun getSenderName() {
    assertEquals("sender name", sendMessageModel.senderName.valueSafe)
  }

  @Test
  fun getSenderAddress() {
    assertEquals("sender@address", sendMessageModel.senderAddress.valueSafe)
  }

  @Test
  fun getRecipientName() {
    assertEquals("recipient name", sendMessageModel.recipientName.valueSafe)
  }

  @Test
  @Disabled("review InternetAddress api to get address string")
  fun getRecipientAddress() {
    assertEquals("recipient@address", sendMessageModel.recipientAddress.name)
  }

  @Test
  fun getSubject() {
    assertEquals("message subject", sendMessageModel.subject.valueSafe)
  }

  @Test
  fun getAttachment() {
    assertNotNull(sendMessageModel.attachment.value)
  }

  @Test
  fun getContent() {
    assertEquals("message content", sendMessageModel.content.valueSafe)
  }

  @Test
  @Disabled("SendMessage should not have a public get method available")
  fun getSend() {
  }

  @Test
  @Disabled("Check if SendMessageModel(val sendMessage = SendMessage()) would impact model")
  fun setSend() {
  }
}
