package model

import io.mockk.mockk
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.MethodOrderer
import org.junit.jupiter.api.Order
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestMethodOrder
import java.io.File
import java.util.Date
import javax.mail.internet.InternetAddress

@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
internal class SendMessageTest {

  private val date: Date = mockk<Date>(relaxed = true)
  private val file: File = mockk<File>(relaxed = true)
  private val sendMessage: SendMessage = SendMessage()

  @BeforeAll
  fun setUp() {
    // sendMessage.id = "one"
    sendMessage.date = date
    sendMessage.senderName = "sender name"
    sendMessage.senderAddress = "sender@address"
    sendMessage.recipientName = "recipient name"
    sendMessage.recipientAddress.add(InternetAddress("recipient@address"))
    sendMessage.subject = "message subject"
    sendMessage.attachment = listOf(file)
    sendMessage.content = "message content"
  }

  @Test
  @Order(1)
  @Disabled
  fun getId() {
    // assertEquals("one", sendMessage.id)
  }

  @Test
  @Order(2)
  @Disabled
  fun getIdProperty() {
    // assertEquals("one", sendMessage.idProperty.valueSafe)
  }

  @Test
  @Order(3)
  fun getDate() {
    assertEquals(date, sendMessage.date)
  }

  @Test
  @Order(4)
  fun getDateProperty() {
    assertEquals(date, sendMessage.dateProperty.value)
  }

  @Test
  @Order(5)
  fun getSenderName() {
    assertEquals("sender name", sendMessage.senderName)
  }

  @Test
  @Order(6)
  fun getSenderNameProperty() {
    assertEquals("sender name", sendMessage.senderNameProperty.valueSafe)
  }

  @Test
  @Order(7)
  fun getSenderAddress() {
    assertEquals("sender@address", sendMessage.senderAddress)
  }

  @Test
  @Order(8)
  fun getSenderAddressProperty() {
    assertEquals("sender@address", sendMessage.senderAddressProperty.valueSafe)
  }

  @Test
  @Order(9)
  fun getRecipientName() {
    assertEquals("recipient name", sendMessage.recipientName)
  }

  @Test
  @Order(10)
  fun getRecipientNameProperty() {
    assertEquals("recipient name", sendMessage.recipientNameProperty.valueSafe)
  }

  @Test
  @Order(11)
  fun getRecipientAddress() {
    assertEquals("recipient@address", sendMessage.recipientAddress)
  }

  @Test
  @Order(12)
  fun getRecipientAddressProperty() {
    assertEquals("recipient@address", sendMessage.recipientAddressProperty.name)
  }

  @Test
  @Order(13)
  fun getSubject() {
    assertEquals("message subject", sendMessage.subject)
  }

  @Test
  @Order(14)
  fun getSubjectProperty() {
    assertEquals("message subject", sendMessage.subjectProperty.valueSafe)
  }

  @Test
  @Order(15)
  fun `attachment is a list of size 1`() {
    assertEquals(1, sendMessage.attachment.size)
  }

  @Test
  @Order(16)
  fun `attachment contains a File`() {
    assertEquals(file, sendMessage.attachment[0])
  }

  @Test
  @Order(17)
  fun `attachment property is list of size 1`() {
    assertEquals(1, sendMessage.attachmentProperty.value.size)
  }

  @Test
  @Order(18)
  fun `attachment property contains a File`() {
    assertEquals(file, sendMessage.attachmentProperty.value[0])
  }

  @Test
  @Order(19)
  fun getContent() {
    assertEquals("message content", sendMessage.content)
  }

  @Test
  @Order(20)
  fun getContentProperty() {
    assertEquals("message content", sendMessage.contentProperty.valueSafe)
  }
}
