package model

import io.mockk.mockk
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Test

internal class InboxTest {

  private val receivedMessage: ReceivedMessage = mockk<ReceivedMessage>(relaxed = true)
  private val inbox: Inbox = Inbox(
    folderName = "INBOX",
    unread = 3,
    messages = listOf(receivedMessage)
  )

  @Test
  fun `folder name does not equal Sent`() {
    assertFalse(inbox.folderName == "Sent")
  }

  @Test
  fun `folder name equals INBOX`() {
    assertEquals("INBOX", inbox.folderName)
  }

  @Test
  fun `unread equals 3`() {
    assertEquals(3, inbox.unread)
  }

  @Test
  fun `message list is of size 1`() {
    assertEquals(1, inbox.messages.size)
  }

  @Test
  fun `message list is a ReceivedPOKO`() {
    assertEquals(receivedMessage, inbox.messages[0])
  }
}
