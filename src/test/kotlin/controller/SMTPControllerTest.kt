package controller

import io.Console
import io.mockk.mockk
import javafx.collections.ObservableList
import model.SendMessage
import model.SendMessageModel
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertDoesNotThrow
import org.junit.jupiter.api.assertThrows
import org.simplejavamail.api.email.Email
import java.io.File
import java.util.Date
import javax.mail.internet.InternetAddress

internal class SMTPControllerTest {

  private val sendMessageModelMock: SendMessageModel = mockk<SendMessageModel>(relaxed = true)
  private val smtpController: SMTPController = SMTPController()

  private fun setUpModel(): SendMessageModel {
    val sendMessage = SendMessage()
    val dateMock: Date = mockk<Date>(relaxed = true)
    val observableListMock: ObservableList<File> = mockk<javafx.collections.ObservableList<File>>(relaxed = true)

    // sendMessage.id = "one"
    sendMessage.date = dateMock
    sendMessage.senderName = "sender name"
    sendMessage.senderAddress = "sender@address"
    sendMessage.recipientName = "recipient name"
    sendMessage.recipientAddress.add(InternetAddress("recipient@address"))
    sendMessage.subject = "message subject"
    sendMessage.attachment = observableListMock
    sendMessage.content = "message content"

    return SendMessageModel(sendMessage)
  }

  @Test
  fun `expected exception testing`() {
    assertThrows<IllegalArgumentException>("Should throw IllegalArgumentException") {
      smtpController.sendMessageBuilder(sendMessageModelMock)
    }
  }

  @Test
  fun `exception absence testing`() {
    assertDoesNotThrow { smtpController.smtpOutlookMailBuilder() }

    assertDoesNotThrow {
      val model: SendMessageModel = setUpModel()
      smtpController.sendMessageBuilder(model)
    }
  }

  @Test
  fun sendMessageBuilder() {
    val model: SendMessageModel = setUpModel()
    val email: Email = smtpController.sendMessageBuilder(model)
    assertNotNull(email)
    assertSame(email, email)
    assertNotSame(email, smtpController.sendMessageBuilder(model))
    Console.println("\n")
    Console.showInfo(email.toString())
  }

  @Test
  @Disabled("Use this method to check for a real connection to smtp mail host")
  fun checkMailConnection() {
  }
}

/*
private val sendMessage = SendMessage()
private val sendMessageModel: SendMessageModel = SendMessageModel(sendMessage)
private val dateMock: Date = mockk<Date>(relaxed = true)
private val fileMock: File = mockk<File>(relaxed = true)
private val sendMessageMock: SendMessage = mockk<SendMessage>(relaxed = true)

fun setUp() {
  val fileMock: File = mockk<File>(relaxed = true)
  val listOfFileMock: List<File> = mockk<List<File>>(relaxed = true)
  val dateMock: Date = mockk<Date>(relaxed = true)


  every { sendMessageMock.id } returns "one"
  every { sendMessageMock.date } returns dateMock
  every { sendMessageMock.senderName } returns "sender name"
  every { sendMessageMock.senderAddress } returns "sender@address"
  every { sendMessageMock.recipientName } returns "recipient name"
  every { sendMessageMock.recipientAddress } returns "recipient@address"
  every { sendMessageMock.subject } returns "message subject"
  every { sendMessageMock.attachment } returns listOfFileMock
  every { sendMessageMock.content } returns "message content"

  every { sendMessageModelMock.id.value } returns sendMessageMock.id
  every { sendMessageModelMock.date.value } returns sendMessageMock.date
  every { sendMessageModelMock.senderName.value } returns sendMessageMock.senderName
  every { sendMessageModelMock.senderAddress.value } returns sendMessageMock.senderAddress
  every { sendMessageModelMock.recipientName.value } returns sendMessageMock.recipientName
  every { sendMessageModelMock.recipientAddress.value } returns sendMessageMock.recipientAddress
  every { sendMessageModelMock.subject.value } returns sendMessageMock.subject
  every { sendMessageModelMock.attachment.value } returns sendMessageMock.attachment
  every { sendMessageModelMock.content.value } returns sendMessageMock.content
}

  @Test
  fun smtpOutlookMailBuilder() {
    val model: SendMessageModel = setUpModel()
    val emailBuilder: EmailPopulatingBuilder = smtpController.sendMailBuilder(model)
    assertNotNull(emailBuilder)
    assertSame(emailBuilder, emailBuilder)
    assertNotSame(emailBuilder, smtpController.sendMailBuilder(model))
  }

  @Test
  fun checkOutlookProperties() {
    smtpController.checkOutlookProperties("USERNAME")
    assertEquals("USERNAME", PropertiesWrapper.props["simplejavamail.smtp.username"] as String)
  }

  @Test
  @Disabled
  fun checkForAttachments() {
    val model: SendMessageModel = setUpModel()
    assertEquals(true, smtpController.checkForAttachments(model))
  }

  @Test
  fun addAttachments() {
    val model: SendMessageModel = setUpModel()
    assertNotNull(model.attachment.value)

    // FIXME: is an empty list [] fix
    val attachments: List<AttachmentResource> = smtpController.addAttachments(model.attachment.value)
    assertNotNull(attachments)
    assertSame(attachments, smtpController.addAttachments(model.attachment.value))
    // assertNotSame(attachments, smtpController.addAttachments(model.attachment.value))
  }
 */
