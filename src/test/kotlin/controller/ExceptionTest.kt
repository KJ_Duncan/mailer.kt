package controller

import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.assertTimeout
import org.simplejavamail.MailException
import java.time.Duration

@Disabled("Until Kirk gets some sleep")
internal class ExceptionTest {

  /* NOTE:[Assertions Junit] https://junit.org/junit5/docs/current/user-guide/#writing-tests-assertions */
  @Test
  @Disabled("For future testing reference")
  fun `some function throws a mail exception`() {
    assertThrows<MailException>("Should throw an exception") {
    }
  }

  @Test
  @Disabled("For future testing reference")
  fun `some function does not time out`() {
    assertTimeout(Duration.ofSeconds(20)) {
    }
  }
}
