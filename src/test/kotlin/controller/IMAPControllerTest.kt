package controller

import StoreBuilder
import io.mockk.mockk
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import model.Inbox
import model.ReceivedMessage
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation
import org.junit.jupiter.api.Order
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestMethodOrder
import org.junit.jupiter.api.assertDoesNotThrow
import org.junit.jupiter.api.assertThrows
import java.util.Date
import javax.mail.internet.InternetAddress

@TestMethodOrder(OrderAnnotation::class)
internal class IMAPControllerTest {

  private val imapController: IMAPController = IMAPController()
  private val mSessionMockRelaxed: StoreBuilder = mockk<StoreBuilder>(relaxed = true)
  private val mSessionMock: StoreBuilder = mockk<StoreBuilder>()

  private val inboxObservableList: ObservableList<Inbox> =
    FXCollections.observableArrayList<Inbox>()

  private fun inboxGenerator() {
    /* FIXME:[IMAPControllerTest::inboxGenerator] not at all acceptable refactor. */
    val receivedMessages = mutableListOf<ReceivedMessage>()
    val mailboxes = listOf<String>("INBOX/ICT310", "INBOX/ICT320", "INBOX/ICT321")
    var n = 0
    var i = 0
    do {
      receivedMessages.add(ReceivedMessage(Date(), listOf(InternetAddress("myemail$n@address.com")), "a subject heading", "some interesting content"))
      if (n % 2 == 1) {
        inboxObservableList.add(Inbox(mailboxes[i++], n, receivedMessages.toList()))
        receivedMessages.clear()
      }
      n++
    } while (n < 6)
  }

  private fun plusOneReceivedMsg(): List<ReceivedMessage> {
    return listOf(ReceivedMessage(Date(), listOf(InternetAddress("myemail1@address.com")), "a subject heading", "some interesting content"))
  }

  /* NOTE:[IMAPController::updateInbox] Reset inbox counts to zero. Filter and update inboxes.
      if inbox in inboxObservableList append new mail to correct inbox via folderName,
      else create a new inbox and append to inboxObservableList. */
  @Test
  fun `InboxObservableList appends new inbox`() {
    /* FIXME:[IMAPControllerTest::InboxObservableList] not at all acceptable refactor. */
    // val q = inboxObservableList.filtered { it.folderName == mailbox }

    inboxGenerator()

    val notNullMailbox = "INBOX/ICT320"
    val nullMailbox = "INBOX/ICT352"

    val msg = plusOneReceivedMsg()
    val inbox = inboxObservableList.find { it.folderName == notNullMailbox }
    if (inbox == null) {
      inboxObservableList.add(Inbox(nullMailbox, 1, msg))
    } else {
      val index = inboxObservableList.indexOf(inbox)
      val n = inboxObservableList[index].unread + msg.size
      val m = inboxObservableList[index].messages + msg
      val w = inboxObservableList.removeAt(index).copy(unread = n, messages = m)
      val z = inboxObservableList.add(w)
    }
  }

  @Test
  @Order(1)
  fun `IMAPController is not null`() {
    assertNotNull(imapController)
  }

  @Test
  @Order(2)
  fun `data fields are not null`() {
    assertNotNull(imapController.imapInboxObservable())
    assertNotNull(imapController.imapSentBoxObservable())
  }

  @Test
  @Order(3)
  fun `expected exception testing`() {
    assertThrows<UninitializedPropertyAccessException> {
      imapController.imapDisconnect()
    }
    assertThrows<UninitializedPropertyAccessException> {
      imapController.imapConnected()
    }
  }

  @Test
  @Order(4)
  fun `imapMailBoxBuilder returns false`() {
    assertEquals(false, imapController.imapMailboxBuilder())
  }

  // @Test
  // @Order(5)
  // fun `imapConnect returns false`() {
  //   assertEquals(false, imapController.imapConnect())
  // }
  //
  // @Test
  // @Order(6)
  // fun `imapConnect returns true`() {
  //   assertEquals(true, imapController.imapConnect())
  // }

  @Test
  @Order(7)
  fun `exception absence testing`() {
    assertDoesNotThrow { imapController.imapDisconnect() }
    assertDoesNotThrow { imapController.imapConnected() }
    assertDoesNotThrow { imapController.imapMailboxBuilder() }
  }

  @Test
  @Order(8)
  fun `imapMailBoxBuilder returns true`() {
    assertEquals(true, imapController.imapMailboxBuilder())
  }
}
