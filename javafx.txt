OpenJFX supports sdk LTS 11 and 14

TornadoFX supports it: https://edvin.gitbooks.io/tornadofx-guide/part1/2_Setting_Up.html

------------------------------------------------------------

OpenJFX: https://wiki.openjdk.java.net/display/OpenJFX/Main

OpenJFX javadoc: https://openjfx.io/javadoc/14/

JavaFX releases: https://gluonhq.com/products/javafx/

OpenJFX samples: https://github.com/openjfx/samples

Gradle javafx plugin: https://plugins.gradle.org/plugin/org.openjfx.javafxplugin
Gradle jlink plugin: https://plugins.gradle.org/plugin/org.beryx.jlink

JavaFX and IntelliJ IDEA: https://openjfx.io/openjfx-docs/

Release Notes for JavaFX 14: https://github.com/openjdk/jfx/blob/jfx14/doc-files/release-notes-14.md

OpenJFX Projects and Components: https://wiki.openjdk.java.net/display/OpenJFX/Projects+and+Components

OpenJFX Runtime images: https://openjfx.io/openjfx-docs/#modular

Understanding a JDK Modular world in our developer build: https://wiki.openjdk.java.net/display/OpenJFX/Building+OpenJFX#BuildingOpenJFX-UnderstandingaJDKModularworldinourdeveloperbuild

The State of the Module System: https://openjdk.java.net/projects/jigsaw/spec/sotms/  <-- openjdk jmod article

JEP 261: Module System, packaging jmod files: http://openjdk.java.net/jeps/261#Packaging:-JMOD-files

The jmod Command: https://docs.oracle.com/en/java/javase/14/docs/specs/man/jmod.html

The jlink Command: https://docs.oracle.com/en/java/javase/14/docs/specs/man/jlink.html


