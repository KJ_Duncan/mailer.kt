### SCHEMA ###

Config file
===========

simplejavamail.properties
--------------------------

simplejavamail.javaxmail.debug=false <-- my choice  
simplejavamail.transportstrategy=SMTPS  
  
simplejavamail.smtp.host=smtp.default.com  
simplejavamail.smtp.port=25  
simplejavamail.smtp.username=username  
simplejavamail.smtp.password=password  

simplejavamail.defaults.subject=  
simplejavamail.defaults.from.name=Kirk Duncan  
simplejavamail.defaults.from.address=kjd016@student.usc.edu.au  
simplejavamail.defaults.replyto.name=Kirk Duncan  
simplejavamail.defaults.replyto.address=kjd016@student.usc.edu.au  
simplejavamail.defaults.to.name=  
simplejavamail.defaults.to.address=  
simplejavamail.defaults.cc.name=  
simplejavamail.defaults.cc.address=  
simplejavamail.defaults.bcc.name=  
simplejavamail.defaults.bcc.address=  
simplejavamail.defaults.poolsize=10  
simplejavamail.defaults.sessiontimeoutmillis=60000  

simplejavamail.transport.mode.logging.only=true <-- or .withTransportModeLoggingOnly()  
simplejavamail.opportunistic.tls=false  

simplejavamail.proxy.host=  
simplejavamail.proxy.port=  
simplejavamail.proxy.username=  
simplejavamail.proxy.password=  
simplejavamail.proxy.socks5bridge.port=  
-----------

Connect via Microsoft Exchange credentials  
https://outlook.office365.com/EWS/Exchange.asmx:443  
port:443  
SSL to connect  

POP setting  
Server name: outlook.office365.com  
Port: 995  
Encryption method: TLS  

IMAP setting  
Server name: outlook.office365.com  
Port: 993  
Encryption method: TLS  

SMTP setting  
Server name: smtp.office365.com  
Port: 587  
Encryption method: STARTTLS  

---

https://outlook.office365.com/mail/sentitems/id/  
https://outlook.office365.com/mail/inbox/id/  
https://outlook.office365.com/people/  
https://outlook.office365.com/calendar/view/month  

---

Microsoft Exchange and Office 365  
https://eclipse-ee4j.github.io/mail/Exchange  

jakarta mail api  
https://eclipse-ee4j.github.io/mail/  
  
e-mail: a valid e-mail address. https://owasp.org/www-community/OWASP_Validation_Regex_Repository  
`^[a-zA-Z0-9_+&*-]+(?:\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,7}$`  

  
---

### Draft ###


```kotlin

class MyView : View("Mailer") {
    val controller: EmailReceivedController by inject()
}

tableview<EmailReceived> {
   items = controller.emailReceived
   isEditable = false

   column("#", EmailReceived::idProperty)
   column("author", EmailReceived::authorProperty)
   column("subject", EmailReceived::subjectProperty).remainingWidth()
   
   smartResize()
}

field("Name") {
    textfield(model.name).required()
}

field("Name") {
    textfield(model.name).validator {
        if (it.isNullOrBlank()) error("The name field is required") else null
    }
}

class EmailReceivedController : Controller() {
    /* val <name> = <type<name>>.observable() */
}


class EmailReceived(id: Int, author: String, subject: String) {
    val authorProperty = SimpleStringProperty(this, "author", author)
    var author by authorProperty
    val subjectProperty = SimpleStringProperty(this, "subject", subject) 
    var subject by subjectProperty
    val idProperty = SimpleIntegerProperty(this, "id", id) 
    var id by idProperty
}

fun loadCustomers() = api.get("customers").list().toModel<Customer>()


/* https://github.com/edvin/tornadofx-samples/blob/master/login/src/main/kotlin/no/tornado/fxsample/login/view.LoginScreen.kt */
fun clear() {
    model.username.value = ""
    model.password.value = ""
    model.remember.value = false
}

val htmlTextProperty: SimpleStringProperty = SimpleStringProperty()
var htmlText: String by htmlTextProperty
val htmlText: SimpleStringProperty = bind { mail.htmlTextProperty }

/* write emails only, HTML is vaunarable to injection attacks do not reply in HTML as resoned above. */
field("Html text", Orientation.VERTICAL) {
  htmleditor(mailItemModel.htmlText.value) {
    vgrow = Priority.ALWAYS
    hgrow = Priority.NEVER
    maxHeight = 250.0
  }
}

.withHTMLText(mailItemModel.htmlText.valueSafe)


/**
* adapted from source:
* @author msgshow.jave
*/
private fun mimeTypeContentBuilder(part: Part, stringBuilder: StringBuilder): Any {
    return when {
      part.isMimeType("text/plain") -> stringBuilder.append(part.content as String)
      part.isMimeType("multipart/*") -> {
        val multipart = part.content as Multipart
        val count: Int = multipart.count
        for (i in 0 until count) mimeTypeContentBuilder(multipart.getBodyPart(i), stringBuilder)
      }
      part.isMimeType("message/rfc822") -> mimeTypeContentBuilder(part.content as Part, stringBuilder)
      else -> stringBuilder.append(part.content.toString())
    }
}

class MailBox(folderName: String, unread: Int, messages: ObservableList<ReceivedItem>) {
  val folderNameProperty = SimpleStringProperty()
  var folderName by folderNameProperty

  val unreadProperty = SimpleIntegerProperty()
  var unread by unreadProperty

  val messagesProperty = SimpleObjectProperty(messages)
  var messages by messagesProperty
}

class ReceivedItem(date: Date, from: List<Address>, subject: String, content: Any) {
  val dateProperty = SimpleObjectProperty(date)
  var date by dateProperty

  val fromProperty = SimpleObjectProperty(from)
  var from by fromProperty

  val subjectProperty = SimpleStringProperty(subject)
  var subject by subjectProperty

  val contentProperty = SimpleObjectProperty(content)
  var content by contentProperty
}

/* TODO:[loadMailProperties] model.ReceivedPOKO::from parse addresses to match form fields */
fun loadMailProperties(receivedPOKO: ReceivedPOKO) {
    booleanProperty.value = false
    mailItemModel.senderAddress.bind(receivedPOKO.from.toString().toProperty())
    mailItemModel.subject.bind(receivedPOKO.subject.toProperty())
    mailItemModel.content.bind(receivedPOKO.content.toString().toProperty())
}

fun resetMailProperties() {
    when {
      mailItemModel.isDirty -> with(mailItemModel) {
        senderAddress.unbind()
        subject.unbind()
        content.unbind()
      }
    }
}

// read all emails in plain text, HTML is vaunarable to injection attacks
// images, attachments and other downloadables are not to be accessesed
// through this program.
field("Plain text", Orientation.VERTICAL) {
    textarea(mailItemModel.content) {
      prefRowCount = 15
      vgrow = Priority.ALWAYS
    }
}

button("Hide F11") {
    enableWhen(booleanProperty)
    action {
      /* TODO:[Reset button] hide child nodes and bounding box size back to 0.0 */
      updateChildNode(null)
    }
    shortcut("F11")
}

// private val toggleGroup: ToggleGroup = ToggleGroup()
radiobutton("20 minutes", toggleGroup)
radiobutton("1 hour", toggleGroup)
radiobutton("2 hour", toggleGroup)
radiobutton("3 hour", toggleGroup).fire()

properties["originalText"] = text
text = "Connecting ..."
opacity = 0.5

text = properties["originalText"] as String
opacity = 1.0

// https://github.com/edvin/tornadofx/blob/master/src/main/java/tornadofx/Async.kt
runAsyncWithOverlay()
runAsyncWithProgress()

textarea(receivedPOKO.content) {
  isEditable = false
  prefRowCount = 15
  vgrow = Priority.ALWAYS
}

private val login = SimpleObjectProperty<Any>()
PropertiesWrapper.props["login-method"] = login.value.toString()
field("Login method") {
  togglegroup(login) {
    radiobutton("Manual")
    radiobutton("Conf")
    radiobutton("Env")
    radiobutton("Props").fire()
  }
}

find<IMAPController>(
  mapOf(IMAPController::class to mSession)
).imapConnect() && find<SMTPController>(
  mapOf(SMTPController::class to mSession)
).smtpConnect()

private enum class USER { MAILER_USERNAME, MAILER_PASSWORD, MAILER_ACCOUNT; }

/* FIXME:[LoginController::checkEnv] use a startup bash script instead of env variables */
private fun checkEnv(): Boolean = kotlin.runCatching {
  System.getenv(USER.MAILER_USERNAME.name).isNullOrBlank() &&
      System.getenv(USER.MAILER_PASSWORD.name).isNullOrBlank() &&
      System.getenv(USER.MAILER_ACCOUNT.name).isNullOrBlank()
}.isFailure  // returns true if all environment variables exist

/* TEST:[LoginController] init function System.getenv */
/* NOTE:[JavaMail API Tutorial] https://www.tutorialspoint.com/javamail_api/index.htm
     http://www.java2s.com/Tutorial/Java/0490__Security/Catalog0490__Security.htm
     https://howtodoinjava.com/security/how-to-generate-secure-password-hash-md5-sha-pbkdf2-bcrypt-examples/ */
init {
  if (checkEnv()) {
    tryLogin(
      System.getenv(USER.MAILER_USERNAME.name),
      System.getenv(USER.MAILER_PASSWORD.name),
      System.getenv(USER.MAILER_ACCOUNT.name)
    )
  } else {
    showLoginScreen()
  }
}
```

---

```
/* Moniters given mailbox for new email
   source wait.java */

folder.addMessageCountListner(new MessageCountAdapter() {
  public void messagesAdded(MessageCountEvent ev) {
    Message[] msgs = ev.getMessages();
    for (Message msg : msgs)
      processMessage(msg);  // user defined method
  }
});
// wait for new message
for (;;) 
  ((IMAPFolder) folder).idle();

```

implementation("de.jensd:fontawesomefx-commons:9.1.2")  
implementation("de.jensd:fontawesomefx-fontawesome:4.7.0-9.1.2")  

---

<https://guides.micronaut.io/micronaut-email-kotlin/guide/index.html>  
  
src/kotlin/main/example/micronaut/EmailService.kt  
  
```
package example.micronaut

interface EmailService {
    fun send(email: Email)
}
```
  
src/kotlin/main/example/micronaut/Email.kt  
  
```
package example.micronaut

interface Email {
  fun recipient(): String?
  fun cc(): List<String>?
  fun bcc(): List<String>?
  fun subject(): String?
  fun htmlBody(): String?
  fun textBody(): String?
  fun replyTo(): String?
}

```
  
https://github.com/JetBrains/kotless/blob/master/model/src/main/kotlin/io/kotless/utils/TypedStorage.kt  
  

```
/** Interface for typeful singleton storage */
class TypedStorage<T>(private val storage: MutableMap<Key<T>, T> = HashMap()) {
    class Key<T>

    val all: Collection<T>
        get() = storage.values

    val entries: Set<Map.Entry<Key<T>, T>>
        get() = storage.entries

    operator fun <K : Key<T>> set(key: K, value: T) {
        storage[key] = value
    }

    operator fun <K : Key<T>> get(key: K): T? {
        return storage[key]
    }

    fun <K : Key<T>> getOrPut(key: K, defaultValue: () -> T): T {
        return storage.getOrPut(key, defaultValue)
    }

    fun addAll(storage: TypedStorage<T>) {
        this.storage.putAll(storage.storage)
    }

    override fun toString(): String {
        return storage.entries.joinToString(prefix = "{", postfix = "}") { "${it.key}: ${it.value}" }
    }
}

```
  
https://github.com/JetBrains/kotless/blob/master/model/src/main/kotlin/io/kotless/HTTP.kt  
  
```
/** Mime types supported by StaticResource */
enum class MimeType(val mimeText: String, val isBinary: Boolean, val extension: String) {
    PLAIN("text/plain", false, "txt"),
    MARKDOWN("text/markdown", false, "md"),
    HTML("text/html", false, "html"),
    CSS("text/css", false, "css"),

    PNG("image/png", true, "png"),
    APNG("image/apng", true, "apng"),
    GIF("image/gif", true, "gif"),
    JPEG("image/jpeg", true, "jpeg"),
    BMP("image/bmp", true, "bmp"),
    WEBP("image/webp", true, "webp"),
    TTF("font/ttf", true, "ttf"),

    JS("application/javascript", false, "js"),
    JSMAP("application/json", false, "map"),
    JSON("application/json", false, "json"),
    XML("application/xml", false, "xml"),
    ZIP("application/zip", true, "zip"),
    GZIP("application/gzip", true, "gzip");

    companion object {
        fun binary() = values().filter { it.isBinary }.toTypedArray()
        fun forDeclaration(type: String, subtype: String) = values().find { "${type}/${subtype}" == it.mimeText }
        fun forFile(file: File) = values().find { it.extension == file.extension }
    }
}
```

---

```
SEVERE: Uncaught error
org.simplejavamail.mailer.MailerException: Invalid FROM address: Email{
    id=null
    fromRecipient=Recipient{name='null', address='dfs', type=null},
    replyToRecipient=null,
    bounceToRecipient=null,
    text='sdfsdfsf',
    textHTML='null',
    subject='dfs',
    recipients=[Recipient{name='null', address='dfs', type=To}]
}
```

---

### Examples ###

/Documents/Programming/Java_Work/mailExamples/

build the first layer MasterDetailPane <https://github.com/controlsfx/controlsfx/wiki/ControlsFX-Features#masterdetailpane>

model.rebind { inbox = otherInbox }

receivedDate: Date
from: Array<Address>
subject: String

view.MailBoxView -> view.TopView -> Button (so i need to create an id for the button to find easily)

---


TornadoFX SwitchView/ViewModel/Validation demo  
<https://youtu.be/V-fNIOLYv88>  

TornadoFX Master Detail using ItemViewModel and Scopes
<https://youtu.be/1G1OYBRDSBs>  

override val scope: Scope = super.scope as EmailSenderScope  
<https://youtu.be/BTZn9m_qMUI?t=143>  


AbstractSessionBuilder https://www.jetbrains.com/help/idea/2019.3/contract-annotations.html  
AbstractSessionBuilder https://www.programcreek.com/java-api-examples/?code=cecid/hermes/hermes-master/piazza-commons/src/main/java/hk/hku/cecid/piazza/commons/net/MailConnector.java  
  
IMAPController::updateInbox see jmail idlemonitor.java or monitor.java implements a message count on updates.  


---

### Thought of the day ###

A whitelist database for email address's to reduce inbox workloads and target expected un-seen/read emails, leaving the rest available through normal mailbox channels.  
  
Database storage for use with PBE.  
  
Non-Tracking Application (NTA) user can send error log files via [Tipbox](https://tipbox.is/). The [java.util.logging](https://docs.oracle.com/en/java/javase/13/docs/api/java.logging/java/util/logging/package-summary.html) APIs offer both static and dynamic configuration control. Static control enables ~~field service staff~~ *a user* to set up a particular configuration and then re-launch the application with the new logging settings.  
  
---


### TO-GO ###


```

FIXME:[NEXT]  
     Types program in Types,
     Validation (see torandofx p.171),
     Scope,
     EventBus (FXEvent),
     How to find a fx node,  
     Testing

NOTE:[resync.java] jakarta mail examples
     L:349 saveAttachments,
     L:182 IMAPFolder,
     L:195 IMAPMessage,
     L:226 FetchProfile,
     L:282 ContentType

TODO:[Project]
     Tests, mocks, coverage, and docstrings
     TaskStatus with runAsync
     resync mail box (see jakartamail resync.java example)
     Contacts lookup on sending message (is Contacts a mail folder I can search for matching user input)
     simplejavamail.properties
     Connect to Gmail account
     Check visability modifiers package/class/methods/fields
     Root node resizing policy
     Logging package wide needs to be implemented (review java security guide dos/donts in logging)
     Rename project to mailer
     Package name org.kjd.mailer
     cmd line launcher and flags -i inbox -s sent view on opening the applicaiton
     launch application via descktop icon
     README.md
     UNLICENSE
     ContactLoader  ✅
     FileErrorManager (see jakarta mail logging dir)  ✅
     PasswordAuthenticator  ✅ <https://www.oracle.com/technetwork/java/seccodeguide-139067.html>
     Annotations (@ExperimentalTime) apply in build.gradle or a package wide place  ✅
     add reply and forward button to received form view  ✅
     attach file to sent message  ✅
     java.prop.Properties  ✅
     Settings view add functionality  ✅
     remove received email html tags  ✅
     popup fragment on error/message  ✅
     jakarta mail properties  ✅
     Radio Button to select mail account  ✅
     MVC package heirarchy  ✅
     Stylesheet (see page 86 mixins for bulk stylings)  ✅
     Connect to MS Exchange account  ✅

TODO:[Views]
      LoginView,
      MasterView,
      TopView,
      CenterView,
      SettingsView,
      InboxTableView,
      SentTableView,
      SentFormView,
      ReceivedFormView,
      SendEditorFragment,  ⏰
      NotificationView
 
```

---
 
