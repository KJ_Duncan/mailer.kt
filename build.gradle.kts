plugins {
  java
  kotlin("jvm") version "1.4.0"
  id("org.jetbrains.dokka") version "0.10.1"
  id("org.openjfx.javafxplugin") version "0.0.9"
}

group = "org.kjd.mailer.kt"
version = "1.0-SNAPSHOT"

repositories {
  mavenCentral()
  jcenter()
}

javafx {
  version = "13"
  // the entire team for development convenience
  modules = listOf(
    "javafx.base",
    "javafx.controls",
    "javafx.fxml",
    "javafx.graphics",
    "javafx.media",
    "javafx.web"
  )
}

// not all dependencies will be used in final application
// all are listed here as a development convenience
// time tells all.
dependencies {
  constraints {
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.3.9")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-javafx:1.3.9")
    
    implementation("no.tornado:tornadofx:1.7.20")
    implementation("no.tornado:tornadofx-controlsfx:0.1.1")
    
    implementation("com.natpryce:result4k:2.0.0")
    
    implementation("org.slf4j:slf4j-simple:1.7.30")
    implementation("io.github.microutils:kotlin-logging:1.8.3")
    
    implementation("com.sun.mail:jakarta.mail:1.6.5")
    implementation("org.simplejavamail:simple-java-mail:6.4.3")
    implementation("org.jsoup:jsoup:1.13.1")
    
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.6.2")
    testImplementation("io.kotest:kotest-runner-junit5:4.2.2")
    testImplementation("io.mockk:mockk:1.10.0")
    
    testImplementation("org.assertj:assertj-core:3.17.1")
    testImplementation("org.assertj:assertj-db:2.0.0")
  }

  dependencies {
    implementation(kotlin("stdlib"))
    
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-javafx")
    
    implementation("no.tornado:tornadofx")
    implementation("no.tornado:tornadofx-controlsfx")
    
    implementation("com.natpryce:result4k")
    
    implementation("org.slf4j:slf4j-simple")
    implementation("io.github.microutils:kotlin-logging")
    
    implementation("com.sun.mail:jakarta.mail")
    implementation("org.simplejavamail:simple-java-mail")
    implementation("org.jsoup:jsoup")

    testImplementation("org.junit.jupiter:junit-jupiter-api")
    testImplementation("io.kotest:kotest-runner-junit5")
    testImplementation("io.mockk:mockk")
    
    testImplementation("org.assertj:assertj-core")
    testImplementation("org.assertj:assertj-db")
  }
}

tasks {
  // https://docs.gradle.org/current/userguide/java_library_plugin.html#sec:java_library_modular
  // Gradle - Building Modules for the Java Module System
  //        - Java Module System support is an incubating feature
  configure<JavaPluginExtension> { modularity.inferModulePath.set(true) }

  configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_13
    targetCompatibility = JavaVersion.VERSION_13
  }

  // kotlin targets 1.6 <= X <= 13
  // if compile target > max, max || min
  compileKotlin {
    kotlinOptions.jvmTarget = "13"
    kotlinOptions.freeCompilerArgs = 
      listOf("-Xuse-experimental=kotlin.time.ExperimentalTime", 
             "-Xinline-classes", 
             "-Xallow-no-source-files")
  }
  
  compileTestKotlin { kotlinOptions.jvmTarget = "13" }

  dokka {
    outputFormat = "html"
    outputDirectory = "$buildDir/javadoc"
    configuration { includeNonPublic = true }
  }

  jar {
    enabled = false
    manifest {
      attributes(
        "Manifest-Version" to "1.0",
        "Created-By" to "kjd016 (Kirk Duncan)",
        "Name" to "org/kjd/mailer",
        "Sealed" to "true",
        "Application-Name" to "mailer",
        "Main-Class" to "org.kjd.mailer.kak.MailerAppKt",
        "Implementation-Title" to "Mailer a light-weight email application",
        "Implementation-Version" to project.version
      )
    }
  }

  test {
    useJUnitPlatform()
    jvmArgs = listOf("-ea")
    testLogging.showStandardStreams = true
    testLogging {
      events("passed",
             "failed",
             "skipped",
             "standard_out",
             "standard_error")
    }
  }
}

/*
val dokkaJar: Jar by tasks.creating(Jar::class) {
  group = JavaBasePlugin.DOCUMENTATION_GROUP
  description = "Assembles the mailer api documentation with dokka"
  archiveClassifier.set("dokka")
  from(tasks.dokka)
}

val uberJar: Jar by tasks.creating(Jar::class) {
  archiveClassifier.set("uber")

  from(sourceSets.main.get().output)
  exclude { details: FileTreeElement ->
    details.file.name.endsWith(".png") ||
      details.file.name.endsWith(".puml")
  }

  dependsOn(configurations.runtimeClasspath)
  from({ configurations.runtimeClasspath.get().filter {
    it.name.endsWith(".jar")}.map { zipTree(it) }})
}

//  `maven-publish`
publishing {
  publications {
    create<MavenPublication>("default") {
      artifact(dokkaJar)
      artifact(uberJar)
    }
  }

  repositories {
    maven { url = uri("$buildDir/repository") }
  }
}
 */
