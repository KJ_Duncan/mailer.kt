### Mailer IMAP/SMTP [TornadoFX](https://tornadofx.io/) Application

  
NOTE  Currently proof-of-concept [pbe-credentials](https://bitbucket.org/KJ_Duncan/pbe-credentials.kt/src/master/) is underway for this repository. Active research, development, and implementation is occurring and will be transposed into this application after unit testing.  
  
Not yet production ready, outstanding tasks:  
  
+ User to App authentication, (pbe or jaas) *WIP
+ Whitelist/PBE embeded db, ([Derby](https://db.apache.org/derby/manuals/index.html), [Derby security](https://builds.apache.org/job/Derby-docs/lastSuccessfulBuild/artifact/trunk/out/security/index.html)) *WIP
+ Test suite non exhaustive
+ Domain driven security
+ Logging with purpose, (an [audit-trail](https://github.com/OpenHFT/Chronicle-Queue) or logging exceptions) 
+ Rate limits and mailbox checks, (api terms of use/service documentation)
+ Gmail implementation ([MTA-STS](https://www.hardenize.com/blog/mta-sts), [MTA-STS gmail](https://security.googleblog.com/2019/04/gmail-making-email-more-secure-with-mta.html))
+ Email encryption ([keytool](https://docs.oracle.com/en/java/javase/14/docs/specs/man/keytool.html), [gnupg](https://keys.openpgp.org/about/usage))
+ Internationalisation, ([id(Indonesian)](https://r12a.github.io/app-charuse/?language=id), [fr-FR(French)](https://r12a.github.io/app-charuse/?language=fr))
+ Method/Function refactoring 
+ Non-functional requirements (Styles.kt)
+ Docstrings, References, and UML diagrams
+ Package application, ([jpackage](https://docs.oracle.com/en/java/javase/14/docs/specs/man/jpackage.html))
+ ~~Decouple logic via an Eventbus (FXEvent)~~
+ ~~Office365 implementation~~ ([STARTTLS](https://starttls-everywhere.org/))


---

  
Author's intention is to move away from the MVC design pattern in favour of the below where possible:  
  
Ghosh, D 2016, Functional and Reactive Domain Modeling, Manning Publications, viewed 19 May 2020, https://www.manning.com/books/functional-and-reactive-domain-modeling.  
  
Common Weakness Enumeration (CWE) 2020, CWE VIEW: Architectural Concepts, A Community-Developed List of Software & Hardware Weakness Types, viewed 24 May 2020, https://cwe.mitre.org/data/definitions/1008.html  
  
---

##### UML - Public / Protected  methods and members

---

![](src/main/resources/img/app.png)  

---

![](src/main/resources/img/view.png)  

---

![](src/main/resources/img/events.png)  

---

![](src/main/resources/img/controller.png)  

---

![](src/main/resources/img/model_io.png)  

---

![](src/main/resources/img/prop.png)  

---

This application should NEVER be used outside of personal use, consider carefully the implications of NOT doing so.  
  
Proofpoint 2020, Email Protection, Proofpoint Email Protection solutions, viewed 16 May 2020, https://www.proofpoint.com/us/products/email-protection  
  
Proofpoint 2020, The State of Email Fraud 2019, Impostor Email Threats Infographic, viewed 16 May 2020, https://www.proofpoint.com/sites/default/files/pfpt-us-ig-the-state-of-email-fraud-2019.pdf  

---

Quick access references:  
  
Common Weakness Enumeration (CWE) 2020, CWE VIEW: Software Development, A Community-Developed List of Software & Hardware Weakness Types, viewed 20 May 2020, https://cwe.mitre.org/data/definitions/699.html  
  
Common Weakness Enumeration (CWE) 2020, 2019 CWE Top 25 Most Dangerous Software Errors, A Community-Developed List of Software & Hardware Weakness Types, viewed 20 May 2020, https://cwe.mitre.org/top25/archive/2019/2019_cwe_top25.html  
  
Jakarta EE 2020, Jakarta Mail Implementation, Jakarta Mail, viewed 23 May 2020, https://eclipse-ee4j.github.io/mail/  
 
Jakarta EE 2020, com.sun.mail.imap, Jakarta Mail, viewed 23 May 2020, https://eclipse-ee4j.github.io/mail/docs/api/com/sun/mail/imap/package-summary.html  
  
Jakarta EE 2020, SSLNOTES.txt, Jakarta Mail, viewed 23 May 2020, https://eclipse-ee4j.github.io/mail/docs/SSLNOTES.txt  
  
Oracle 2019, Trail: Internationalization, The Java Tutorials, viewed 25 May 2020, https://docs.oracle.com/javase/tutorial/i18n/index.html  
  
Apache James 2020, Java Apache Mail Enterprise Server, viewed 31 August 2020, http://james.apache.org/index.html  
  
